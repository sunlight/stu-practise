define(['text!pickPersonHtml/comment.html'],
	function(viewTemplate){
		return Piece.View.extend({
			id:"comment",
			events:{
				"click .backbtn":"goBack"
			},
			render:function(){
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow:function(){
				var me = this;
				me.init();//初始化数据
			},
			init:function(){
				var me = this;
				var type = Piece.Session.loadObject("type");
				if (type=="eating") {
					//获取保存的对象datas
					var eatImfor = Piece.Session.loadObject("eatingImfor");
					me.createEatingComment(eatImfor,type);
				}else if (type=="watchingMovies") {
					var moviesImfor = Piece.Session.loadObject("watchingImfor");
					me.creatWatchMoviesComment(moviesImfor,type);
				}else if (type=="traveling") {
					var travelImfor = Piece.Session.loadObject("travelingImfor");
					me.creatTravelComment(travelImfor,type);
				}else if (type=="carPool") {
					var carPoolImfor = Piece.Session.loadObject("carPoolImfor");
					me.creatCarComment(carPoolImfor,type);
				};
			},
			creatCarComment:function(carPoolImfor,type){
				var me = this;
				me.share(carPoolImfor);
				var id = carPoolImfor.id;
				//获取评论数据
				me.getCommentDatas(id,type);
			},
			creatTravelComment:function(travelImfor ,type){
				var me = this;
				me.share(travelImfor);
				var id = travelImfor.id;
				//获取评论数据
				me.getCommentDatas(id,type);
			},
			creatWatchMoviesComment:function(moviesImfor,type){
				var me = this;
				me.share(moviesImfor);
				var id = moviesImfor.id;
				//获取评论数据
				me.getCommentDatas(id,type);
			},
			share:function(obj){//把评论数字和浏览量值放入评论导航里
				var me = this;
				//eatImfor = JSON.parse(eatImfor);
				// var nickName = eatImfor.nickName;
				// var headPortraint = eatImfor.headPortraint;
				var pageViews = obj.pageViews;
				var reviews = obj.reviews;
				me.$el.find(".comment-number").append(reviews);
				me.$el.find(".pageViews-number").append(pageViews);
			},
			createEatingComment:function(eatImfor,type){
				var me = this;
				me.share(eatImfor);
				var id = eatImfor.id;
				//获取评论数据
				me.getCommentDatas(id,type);
			},
			getCommentDatas:function(id,type){
				var me = this;
				$.getJSON("../"+type+"/"+id+"/comment/pinglun.json", function(json) {
					//添加template到ul中
					var template = _.template(this.$("#comment-template").html(),json);
					me.$el.find('.comment-content').append(template);
				});
			},
			goBack:function(){
				window.history.back();
			}
		});
	});