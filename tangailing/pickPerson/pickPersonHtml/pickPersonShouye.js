define(['text!pickPersonHtml/pickPersonShouye.html','shared/js/datasource','listview/ListView','swipe','zepto'],
	function(viewTemplate,DataSource,ListView,swipe,$){
		return Piece.View.extend({
			id:"pickPersonShouye",
			events:{
				"click .show-topic":"goToDetails"
			},
			render:function(){
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow:function(){
				var me = this;
				//初始化正在捡人列表
				//加载数据
				$.getJSON("../pickingPerson/pickingPerson.json", function(json) {
					//添加template到ul中
					var template = _.template(this.$("#pickingPerson-template").html(),json);
					me.$el.find('#myListView').append(template);
				});
				// $.ajax({
				// 	url: '../pickingPerson/pickingPerson.json',
				// 	success: function(data) {
				// 		//var obj = JSON.parse(data);
				// 		alert(data.name);
				// 		var template = _.template(this.$("#pickingPerson-template").html(),data);
				// 		me.$el.find('.table-li').append(template);
				// 	}
				// });
				// me.configIscroll = new IScroll('#configWrapper', {
				// 	mouseWheel: true,
				// });
			},
			goToDetails:function(el){
				var me = this;
				//alert("aa");
				var $target = $(el.currentTarget);
				var dataValue = $target.attr("data-value");
				if (dataValue == "chifan") {
					this.navigate('/pickPersonHtml/eating', {
						trigger: true
					});
				} else if (dataValue == "kandianying") {
					this.navigate('/pickPersonHtml/watchingMovies', {
						trigger: true
					});
				}
				else if (dataValue == "lvyou") {
					this.navigate('/pickPersonHtml/traveling', {
						trigger: true
					});
				}else if (dataValue == "pingche") {
					this.navigate('/pickPersonHtml/carPool', {
						trigger: true
					});
				}	
			}
		});
	});