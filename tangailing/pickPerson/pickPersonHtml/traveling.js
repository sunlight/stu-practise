define(['text!pickPersonHtml/traveling.html'],
	function(viewTemplate){
		return Piece.View.extend({
			id:"traveling",
			X:0,//图片x抽的位置
			Y:0,
			imgX:0,//图片的宽度
			imgY:0,
			scrollY:0,//网页被卷取的高
			events:{
				"click #comment":"goToComment",
				"click .backbtn":"goBack",
				"click .myimg":"changeSizeBig",
				"click .ceng-img":"changeSizeSmall",
				"click .eat-details":"showAll",
			},
			render:function(){
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow:function(){
				var me = this;
				me.getImfor();
			},
			getImfor:function(){
				var me = this;
				//加载数据
				$.getJSON("../traveling/traveling.json", function(json) {
					//添加template到ul中
					var template = _.template(this.$("#eating-template").html(),json);
					me.$el.find('.all-imfor').append(template);
				});
			},
			goToComment:function(el){
				var me = this;
				//获得当前单击目标
				var $target = $(el.currentTarget);
				var datas = $target.attr("data-json");
				datas = JSON.parse(datas);
				//存储datas
				Piece.Session.saveObject("travelingImfor",datas);
				Piece.Session.saveObject("type","traveling");
				this.navigate('comment',{
					trigger:true
				});
			},
			showAll:function(el){
				var me =  this;
				var $target = $(el.currentTarget);
				$target.removeClass("eat-details");
				$target.addClass("eat-details1");
			},
			changeSizeSmall:function(el){
				var me = this;
				//alert(me.X+" "+me.Y);
				var me = this;
				me.$el.find(".ceng-img").animate({
					width:0+'px',
					height:0+'px',
					top:me.Y-scrollY-me.imgY/2+'px',
					left:me.X+me.imgX/2+'px',
				},300);
			},
			changeSizeBig:function(el){
				var me = this;
				//获得当前单击目标
				var $target = $(el.currentTarget);
				//获取图片的宽度
				me.imgX = $target.width();
				me.imgY = $target.height();
				//获取当前图片的位置
				me.scrollY=document.body.scrollTop;
				me.X = $target.offset().left;
				me.Y = $target.offset().top; 
				//alert(X+"  "+Y);
				$(".ceng-img").css({"top":me.Y-scrollY-me.imgY/2,"left":me.X+me.imgX/2});
				var url = $target.attr('src');
				//显示图片放大的层;
				$(".change-img").attr("src",url);
				// img.css("display","inline-block");
				// me.$el.find(".ceng-img").html(img);
				me.$el.find(".ceng-img").animate({
					width:100+'%',
					height:100+'%',					
					top:0+'px',
					left:0+'px',
				},300);
			},
			goBack:function(){
				window.history.back();
			}
		});
	});