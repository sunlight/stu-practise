var Piece;
//pieceDefaultConfig
var pieceDefaultConfig = {
	loadFrom: "module",
	defaultModule: null,
	defaultView: null,
	loadMode: "view",
	enablePad: false,
	hideAddressBar: true,
	enablePhoneGap: false,
	preventTouchMove: false,
	bodyEl:"body",
};

var pieceConfig;

if (typeof(pieceConfig) === "undefined") {
	pieceConfig = new Object();
}

if (pieceConfig.enablePhoneGap === undefined) {
	pieceConfig.enablePhoneGap = false;
}

//如果没有自定义，或者定义了但是不是module，那么默认是从根目录进入
if (pieceConfig.loadFrom === "root") {
	require.config({
		baseUrl: '.',
	});
} else {
	require.config({
		baseUrl: '../',
	});
}

require.config({
	paths: {
		//plugin
		text: 'piece/js/vendor/requirejs-text/js/text',
		domReady: 'piece/js/vendor/requirejs-domready/js/domready',
		i18n: 'piece/js/vendor/requirejs-i18n/js/i18n',

		moment: 'piece/js/vendor/moment/moment',

        css: 'piece/js/vendor/require-css/css',

		zepto: 'piece/js/vendor/zepto/js/zepto',
		underscore: 'piece/js/vendor/underscore/js/underscore',
		backbone: 'piece/js/vendor/backbone/js/backbone',
		fastclick: 'piece/js/vendor/fastclick/js/fastclick',
        iscroll: 'piece/js/vendor/iscroll/js/iscroll-probe',
        spin: 'piece/js/vendor/spinjs/spin',

        swipe: 'piece/js/vendor/swipe/js/swipe',

        core: 'piece/js/core',
        components: 'piece/js/components',
        listview: 'piece/js/components/listview',
	},
	waitSeconds: 30,
	shim: {
		backbone: {
			deps: ['underscore'],
			exports: 'Backbone'
		},
		iscroll: {
			exports: 'IScroll'
		},
		zepto: {
			exports: '$'
		},
		underscore: {
			exports: '_'
		},
		fastclick: {
			exports: 'FastClick'
		}
	}
});

//设置默认语言
if (window.localStorage['lang'] === undefined) window.localStorage['lang'] = "zh-cn";
requirejs.config({
	config: {
		i18n: {
			locale: window.localStorage['lang']
		}
	}
});
(function() {

	//navigation api(web), will be replace 
	window.Navigation = window.navigation || {};
	window.Navigation.navigate = function(fragment, options) {
		options = options || {trigger: true};
		Backbone.history.navigate(fragment, options);
	};
	//delegate to window.location
	window.Navigation.href = function(href) {
		window.location.href = href;
		//TODO: open new tab/window mode?
	};
	//delegate to backbone history navigate
	window.Navigation.backboneNavigate = function(fragment, options) {
		options = options || {trigger: true};
		Backbone.history.navigate(fragment, options);
	};

	require(['zepto', "underscore", "backbone", "fastclick", "text", "i18n", "components/components","core/app"],
		function($, _, Backbone, FastClick, text, i18n, Components, App) {
			Piece = Components;
			pieceConfig = _.extend(pieceDefaultConfig, pieceConfig);
			
			//hide address bar
			if (pieceConfig.hideAddressBar) setTimeout(function() {
				window.scrollTo(0, 1);
			}, 0);
			if (pieceConfig.preventTouchMove) document.addEventListener('touchmove', function(e) {
				e.preventDefault();
			}, false);

			function onDeviceReady(desktop) {

				// Hiding splash screen when app is loaded
				window.isDesktop = desktop;
				FastClick.attach(document.body);

				App.initialize({el:pieceConfig.bodyEl});

				// $('html').css('min-height', window.screen.availHeight - 44 + "px");
				// $('html').css('min-height', window.innerHeight);

			}

			$(pieceConfig.bodyEl).ready(function(){
				onDeviceReady(true);
			});
			// }); //domReady
		});
})();