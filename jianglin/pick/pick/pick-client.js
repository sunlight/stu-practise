define([], function() {
	return {
		
		path_pick_travel_list: "../data/travel/data.json",
		path_pick_eat_list: "../data/eat/data.json",
		path_pick_movie_list: "../data/movie/data.json",
		path_pick_carpool_list: "../data/carpool/data.json",
		path_pick_review_list:"../data/review/data.json"
	};
});