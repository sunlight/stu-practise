define([
		'text!pick/pickMainPage.html',
		'swipe',
		'spin',
		'shared/js/client'
	],
	function(viewTemplate, swipe, Spinner, Client) {
		return Piece.View.extend({
			id: 'pickMainPage',
			events: {
				"click .travel,.eat,.movie,.carpool": "goToDetail",
				
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				
				
			},
			goToDetail: function(el) {
				var me = this;
				Piece.Session.saveObject("isGoBack", true);
				var $target = $(el.currentTarget);
				var dataValue = $target.attr("data-value");
				/*dataJson = JSON.parse(dataJson);
				dataJson.type = me.options.from;
				dataJson = JSON.stringify(dataJson);*/
				// Piece.Session.saveObject("carsDetail_dataJson", dataJson);
				if (dataValue == "travel") {
					this.navigate('/pick/travel', {
						trigger: true
					});
				} else if (dataValue == "eat") {
					this.navigate('/pick/eat', {
						trigger: true
					});
				} else if (dataValue == "movie") {
					this.navigate('/pick/movie', {
						trigger: true
					});
				} else if (dataValue == "carpool") {
					this.navigate('/pick/carpool', {
						trigger: true
					});
				} 

			}

		}); //view define

	});