define([
		'text!pick/pickList.html',
		'testDrive/index',
		'shared/js/datasource',
		'listview/ListView',
		'pick/pick-client',
		'zepto'
	],
	function(viewTemplate, TestDrive, DataSource, ListView, pickClient,$) {
		return Piece.View.extend({
			id: 'pickList',
			events: {
				"click .reviews": "goToReviews",
				"click .ItineraryDetialImg img": "goToImg",
				"click .ItineraryDetialText": "goToTxt",
				
				
			},
			render: function() {
				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
				if (me.options.from == "travel") {
					url = pickClient.path_pick_travel_list;
					template = "travel-template";
				} else if (me.options.from == "eat") {
					url = pickClient.path_pick_eat_list;
					template = "eat-template";
				}else if (me.options.from == "movie") {
					url = pickClient.path_pick_movie_list;
					template = "movie-template";
				}else if (me.options.from == "carpool") {
					url = pickClient.path_pick_carpool_list;
					template = "carpool-template";
				}else if (me.options.from == "review") {
					url = pickClient.path_pick_review_list;
					template = "review-template";
				}





				
				// if (me.options.isCarsDetailReturn && me.options.currentIndex == me.options.lastIndex) {//index比较是为了state相互不影响
				if (me.options.isCarsDetailReturn) {
					me._initListView(me.options.currentIndex, "session", true, url, template);
					Piece.Session.saveObject("isCarsDetailReturn", false);
				} else {
					me._initListView(me.options.currentIndex, "session", false, url, template); //打上缓存标志session
					me.listview.reloadData(); //重新load数据，由于标志session所以缓存数据
				}


			},
			//初始化iscroll列表
			_initListView: function(identifier, storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'carmarket-list' + identifier,
					storage: storage,
					url: url,
					pageParam: 'pageIndex',

				});


				var mainTemplate = _.template($("#pick-list-template").html());
				var listDiv = mainTemplate({
					id: "carmarket-list" + identifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#carmarket-list" + identifier);
				var template = _.template(this.$("#" + template).html());
				//初始化垂直列表
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "carmarket-list" + identifier
				});

			},
			goToReviews: function(el) {
				
				// var me = this;
				// var $target = $(el.currentTarget);
				// var dataJson = $target.attr("data-json");
				// dataJson = JSON.parse(dataJson);
				// dataJson.type = me.options.from;
				// dataJson = JSON.stringify(dataJson);
				// me.listview.saveState();
				// Piece.Session.saveObject("carsDetail_dataJson", dataJson);
				this.navigate('/pick/review', {
					trigger: true
				});
			},
			goToImg: function(evt) {
				


			
				var imgTag=(window.event)?event.srcElement:evt.target;
    			var imgPath=imgTag.src.replace(/\_\d\./,"_4.");//取得弹出的大图url
    			var tagTop=Math.max(document.documentElement.scrollTop,document.body.scrollTop);
    			
       			$(".hidden").css({'height':Math.max(document.body.clientHeight,document.body.offsetHeight,document.documentElement.clientHeight)+'px',"display":"block"});
       			$(".hiddenImg").css({'left':(parseInt(document.body.offsetWidth)/2-38)+'px','top':(document.documentElement.clientHeight/3+tagTop)+'px',"display":"block"});
        		$(".hiddenImg").append("<img id='hiddenImg'  src="+imgPath+" style='width: 77px;height: 65px' />")
				var tx=$("#hiddenImg").attr("width"),ty=$("#hiddenImg").attr("height");
				var sx=document.documentElement.clientWidth,sy=window.innerHeight||document.documentElement.clientHeight;
				var ix=sx;
				var iy=sx*ty/tx;
				var x=tx;var y=ty;
				imgSetTimeOut = setInterval(function() {

					if(x<=ix){
						$("#hiddenImg").css({'width':x+'px','height':y+'px','margin-top':'-65px','margin-left': '-122px'});
						x=1+x;
						y=y+x/y;
					}else{
						clearInterval(imgSetTimeOut);
					}

				}, 10); 



    },
    		
    		imgHidden1:function(){
    			alert("11");

    		},

			goToTxt: function(el) {
				var txtId=$(el.currentTarget).attr("id")
				$("#"+txtId).css({'-webkit-line-clamp':'100'})
				
				
			},
			
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});