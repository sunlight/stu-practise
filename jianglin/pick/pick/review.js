define([
		'text!pick/review.html', 'pick/pickList', 'shared/js/client', 'zepto'
	],
	function(viewTemplate, pickList, Client, $) {
		return Piece.View.extend({
			id: 'review',
			events: {
				"click .backbtn": "goBack"
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			init: function() {
				var me = this;
				//初始化tab内容
				var isCarsDetailReturn = Piece.Session.loadObject("isCarsDetailReturn");
				Piece.Session.saveObject("isCarsDetailReturn", false);
				var content = $(me.el).find(".review-content");
				me.review = new pickList({
					el:content,
					from: "review",
					isCarsDetailReturn: isCarsDetailReturn,
					// lastIndex: lastIndex,
					currentIndex: 2
				});
				me.review.render();

				//默认显示车系简介
				// me.showTabItem(me.commercialCars, 0);

			},
			showTabItem: function(tabItem, tabIndex) {
				var me = this;
				if (tabItem) {
					tabItem.refresh(); //调用新选择的页面的refresh方法。
					$(me.el).find("li.tab-item").removeClass("active");
					$(me.el).find("li#" + tabItem.displayId).addClass("active");
					$(me.el).find(".scroll-lite span").animate({
						left: tabIndex * 50 + '%'
					}, 300, 'linear');
					// $(tabItem.el).css("display", "inherit");
				}

				// me.activeTab = tabItem;

			}

		}); //view define

	});