define(['shared/js/client'], function(BaseClient){
	var basePath = "http://m.changan.com.cn";
	return _.extend(BaseClient, {
		path_carTypePVList: basePath + "/cameap/baidu/api/pv/cars.json",
		path_carTypeCVList: basePath + "/cameap/baidu/api/cv/cars.json",
		path_provincePVList: basePath + "/cameap/baidu/api/pv/province.jsp",
		path_provinceCVList: basePath + "/cameap/baidu/api/cv/province.jsp",
		path_cityPVList: basePath + "/cameap/baidu/api/pv/city.jsp",
		path_cityCVList: basePath + "/cameap/baidu/api/cv/city.jsp",
		path_dealerPVList: basePath + "/cameap/baidu/api/pv/dealer.jsp",
		path_dealerCVList: basePath + "/cameap/baidu/api/cv/dealer.jsp",
		path_testDrivePV: basePath + "/cameap/baidu/api/pv/testDrive.jsp",
		path_testDriveCV: basePath + "/cameap/baidu/api/cv/testDrive.jsp",

		getCarStyleList: function(params) {			//获得车型列表
			var url = params.type == "pv" ? this.path_carTypePVList :　this.path_carTypeCVList;
			this.ajax({
				url: url,
				success: params.success,
				error: params.error,
				complete: params.complete,
				beforeSend: params.beforeSend
			});
		},
		getProvinceList: function(params) {				//获得省份列表
			var url = params.type == "pv" ? this.path_provincePVList : this.path_provinceCVList;
			this.ajax({
				url: url,
				success: params.success,
				error: params.error,
				complete: params.complete,
				beforeSend: params.beforeSend
			});
		},
		getCityList: function(params) { 			//获得城市列表
			var url = params.type == "pv" ? this.path_cityPVList : this.path_cityCVList;
			console.log(params.id);		
			this.ajax({
				url: url,
				data: {
					provinceId: params.id,
				},
				success: params.success,
				error: params.error,
				complete: params.complete,
				beforeSend: params.beforeSend
			});
		},
		getDealerList: function(params) {			//获得经销商列表
			var url = params.type == "pv" ? this.path_dealerPVList : this.path_dealerCVList;
			this.ajax({
				url: url,
				data: {
					cityId: params.id,
				},
				success: params.success,
				error: params.error,
				complete: params.complete,
				beforeSend: params.beforeSend
			});
		},
		testDrive:function(params) {			//保存预约试驾
			var url = params.type == "pv" ? this.path_testDrivePV : this.path_testDriveCV;
			this.ajax({
				url: url,
				type:"post",
				data: {
					"provinceId": params.provinceId,
					"cityId": params.cityId,
					"dealerId": params.dealerId,
					"carId": params.carId,
					"name": params.name,
					"mobile": params.mobile,
					"email": params.email,
					"date": params.date,
					"txt": params.txt,
				},
				success: params.success,
				error: params.error,
				complete: params.complete,
				beforeSend: params.beforeSend
			});
		}

	});
});