define([
		'text!app/comment-detail.html', 'app/comment-list', 'shared/js/client', 'zepto'
	],
	function(viewTemplate, ContentList, Client, $) {
		return Piece.View.extend({
			id: 'comment-list-content',
			events: {
				"click .backbtn": "goBack",
				"click .btn-primary": "hideForm",
					
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			hideForm:function(e){
				
				$(".reply-box").addClass("hideForm");
				setTimeout(function(){
					$("nav.reply-box").css("display","none");
					$(".reply-box").removeClass("hideForm");
				},1000
			);	
			},
			init: function() {
				var me = this;
				//初始化tab内容
				var isCarsDetailReturn = Piece.Session.loadObject("isCarsDetailReturn");
				Piece.Session.saveObject("isCarsDetailReturn", false);
				var content = $(me.el).find("div.comment-container");
				me.passengerCars = new ContentList({
					el: content,
					from: "pv",
					isCarsDetailReturn: isCarsDetailReturn,
					// lastIndex: lastIndex,
					currentIndex: 1
				});
				me.passengerCars.render();

			}

		}); //view define

	});