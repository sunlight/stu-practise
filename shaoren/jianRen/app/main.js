define([
		'text!app/main.html',
		'swipe',
		'spin',
		'shared/js/client'
	],
	function(viewTemplate, swipe, Spinner, Client) {
		return Piece.View.extend({
			id: 'main',
			events: {
				"click .theme-item-img": "goToDetail",
				// "click #changanImpression,#passengerCars,#commercialCars,#testDrive,#cusSerOnline": "goToDetail",
			},
			render: function() {

				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
			},
			goToDetail: function(el) {
				var me = this;
				Piece.Session.saveObject("isGoBack", true);
				var $target = $(el.currentTarget);
				var dataValue = $target.attr("data-value");
				/*dataJson = JSON.parse(dataJson);
				dataJson.type = me.options.from;
				dataJson = JSON.stringify(dataJson);*/
				// Piece.Session.saveObject("carsDetail_dataJson", dataJson);
				if (dataValue == "car") {
					this.navigate('/app/content', {
						trigger: true
					});
				} else if (dataValue == "movice") {
					this.navigate('/app/content', {
						trigger: true
					});
				} else if (dataValue == "food") {
					this.navigate('/app/content', {
						trigger: true
					});
				} else if (dataValue == "travel") {
					this.navigate('/app/content', {
						trigger: true
					});
				} 

			}

		}); //view define

	});