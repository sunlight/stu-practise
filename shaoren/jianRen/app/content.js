define([
		'text!app/content.html', 'app/contentList', 'shared/js/client', 'zepto'
	],
	function(viewTemplate, ContentList, Client, $) {
		return Piece.View.extend({
			id: 'JianRen-content',
			events: {
				"click .backbtn": "goBack"
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			init: function() {
				var me = this;
				//初始化tab内容
				var isCarsDetailReturn = Piece.Session.loadObject("isCarsDetailReturn");
				Piece.Session.saveObject("isCarsDetailReturn", false);
				var content = $(me.el).find(".JianRen-content");
				me.passengerCars = new ContentList({
					el: content,
					from: "pv",
					isCarsDetailReturn: isCarsDetailReturn,
					// lastIndex: lastIndex,
					currentIndex: 1
				});
				me.passengerCars.render();

			}

		}); //view define

	});