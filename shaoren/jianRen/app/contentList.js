define([
		'text!app/contentList.html',
		'shared/js/datasource',
		'listview/ListView',
		'app/carmarket-client',
        'app/js/alert-box',
        'app/js/swiper.min'
	],
	function(viewTemplate,  DataSource, ListView, Client,AlertBox,Swiper) {
		return Piece.View.extend({
			id: 'content-list',
			events: {
				"click .show-detail": "goToCommentDetail",
                "click p.content-txt":"showMoreText",
                "click .show-alert":"showImages",
                "click .tinymask":"hideImages",
			},
			hideImages:function(e){

				$("#tinybox").css("-webkit-animation","hideImges 0.4s ease-out");
				$("#tinybox").css(" animation","hideImges 0.4s ease-out");

				setTimeout(function(){
					$(e.currentTarget).css("display","none");
					$(".tinymask").css("display","none");
					$("#tinybox").css("display","none");
				},400
				);	
			},
            showImages:function(e){
                var url_img_array = [];
                var current_imges_index = 0;
                var images_index = 0;
                var current_url_imges = $(e.currentTarget).find("img").attr("src");

                var currentImg ="";
                    $($(e.currentTarget).parent("ul").find("li")).each(function(index,element){

                        var temp = $(element).find("img").attr("src");
                        if(temp==current_url_imges)
                        {
                            current_imges_index = index;// 确定当前图片的索引
                        }
                        url_img_array[index] = temp;
                        currentImg += "<div class='swiper-slide'><img width='100%' height='100%' src='"+url_img_array[index]+"'/></div>";
                    });


                /* 禁止滚动*/
                document.body.addEventListener("touchmove",function(e){
                	e.preventDefault();
                	return false;
                });

                $(".showPreload").css("display","block");// 加载图标div显示

				// 添加动画
				$("#tinybox").css("-webkit-animation","showBigImges 1s ease-out 0.3s");
				$("#tinybox").css(" animation","showBigImges 1s ease-out 0.3s");
                $(".tinymask").css("display","block");
				$("#tinybox").css("display","block");

				$(".swiper-wrapper").empty();// 清空slide，以防重叠

				$(".swiper-wrapper").append(currentImg);
					var swiper = new Swiper('.swiper-container',{
					lazyLoading : true,
					lazyLoadingOnTransitionStart : true,
					pagination : '.pagination',
					paginationClickable :true
				 });
                 swiper.slideTo(current_imges_index, 1, false);// 滑动到当前的图片

				setTimeout(function(){
					$(".showPreload").css("display","none");//隐藏加载图标
					$("#tinybox").css("opacity",1);// 设置透明度
				},1000);
				$("#tinybox").css("opacity",0);// 设置透明度
            },
			render: function() {

				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			setScreenHeight:function(){
				var height = window.screen.height-50;
				if(height>0){
					$("#carmarket-list1").css("height",height);
				}	
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
				if (me.options.from == "pv") {
					url = Client.url;
					template = "content-template";
				} 
	
				if (me.options.isCarsDetailReturn) {
					me._initListView(me.options.currentIndex, "session", true, url, template);
					Piece.Session.saveObject("isCarsDetailReturn", false);
				} else {
					me._initListView(me.options.currentIndex, "session", false, url, template); //打上缓存标志session
					me.listview.reloadData(); //重新load数据，由于标志session所以缓存数据
				}

			},
			//初始化iscroll列表
			_initListView: function(identifier, storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'carmarket-list' + identifier,
					storage: storage,
					url: url,
					pageParam: 'pageIndex'

				});

				var mainTemplate = _.template($("#content-list-template").html());
				var listDiv = mainTemplate({
					id: "carmarket-list" + identifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#carmarket-list" + identifier);
				var template = _.template(this.$("#" + template).html());

				this.setScreenHeight();
				//初始化垂直列表
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "carmarket-list" + identifier
				});
			},
            showMoreText:function(e){
                var isContentEllipsis = $(e.currentTarget).hasClass("content-txt-ellipsis");
                if(isContentEllipsis)
                {
                    $(e.currentTarget).removeClass("content-txt-ellipsis");
                }else{
                    $(e.currentTarget).addClass("content-txt-ellipsis");
                }

            },
			goToCommentDetail: function(el) {
				var me = this;
				/* 把对象传递到另一个页面*/
				var $target = $(el.currentTarget);
				var dataJson = $target.attr("data-json");

				dataJson = JSON.parse(dataJson);
				dataJson.type = me.options.from;
				dataJson = JSON.stringify(dataJson);
				me.listview.saveState();
				Piece.Session.saveObject("carsDetail_dataJson", dataJson);


				this.navigate('/app/comment-detail', {
					trigger: true
				});
			},
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});