define([
		'text!app/comment-list.html',
		'shared/js/datasource',
		'listview/ListView',
		'app/carmarket-client'
	],
	function(viewTemplate,  DataSource, ListView, Client) {
		return Piece.View.extend({
			id: 'content-list',
			events: {
				"click .show-reply-input": "showInput",
				"click .submit-input": "submitInput"
			},
			render: function() {

				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			setScreenHeight:function(){
				var height = window.screen.height-104;
				if(height>0){
					$("#carmarket-list1").css("height",height);
				}
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
				if (me.options.from == "pv") {
					url = Client.comment;
					template = "comment-template";// 每个条目的模板
				} 
	
				if (me.options.isCarsDetailReturn) {
					me._initListView(me.options.currentIndex, "session", true, url, template);
					Piece.Session.saveObject("isCarsDetailReturn", false);
				} else {
					me._initListView(me.options.currentIndex, "session", false, url, template); //打上缓存标志session
					me.listview.reloadData(); //重新load数据，由于标志session所以缓存数据
				}

			},
			//初始化iscroll列表
			_initListView: function(identifier, storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'carmarket-list' + identifier,
					storage: storage,
					url: url,
					pageParam: 'pageIndex'

				});

				var mainTemplate = _.template($("#comment-list-template").html());// 取出评论块模板
				var listDiv = mainTemplate({
					id: "carmarket-list" + identifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#carmarket-list" + identifier);
				var template = _.template(this.$("#" + template).html());

				this.setScreenHeight();
				//初始化垂直列表
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "carmarket-list" + identifier
				});
			},
            submitInput:function(){

            }
            ,
            showInput: function(el) {
                var currentBox = $("nav.reply-box").css("display");
                if(currentBox =="none")
                {
                    $("nav.reply-box").css("display","block");
                }else
                {
                    $("nav.reply-box").css("display","none");
                }
			},
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});