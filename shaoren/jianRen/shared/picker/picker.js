define(['zepto', 'underscore', 'backbone', 'text!../picker/picker.html', 'iscroll', 'moment', 'css!../picker/picker'], function($, _, Backbone, viewTemplate, IScroll, Moment) {
	var Menu = Backbone.View.extend({
		events: {
			"click #picker": "pickcancleClick",

		},
		initialize: function(el, that) {


			// el 当前页面
			this.that = that;
			this.render(el);

		},
		render: function(el) {
			var me = this;
			$(this.el).html(viewTemplate);
			$(el).append($(this.el));
		},

		that: null,

		dscrollPosition: null, //无限增加元素滚轴 初始化位置
		dselectIndex: null, //无限增加元素滚轴 初始化选中元素的所在数组中的index
		//显示picker控件
		showPicker: function(data) {


			this.initIscroll(data);
			$("#picker").show();

			for (var i = 0; i < this.iscrollArray.length; i++) {

				this.iscrollArray[i].refresh();
			}


		},

		//初始化渲染滚轴

		initIscroll: function(data) {
			this.iscrollArray = new Array();

			$(".pickerList").html('');

			var offw = (270 - data.length + 1) / data.length + 'px';



			for (var i = 0; i < data.length; i++) {



				var newht = '<div id="wrapper' + i + '"style="width:' + offw + ';"class="pickerContainer wrappers" > <div  class="scrollers"><div class="pickeritem' + i + '" ></div></div></div>';



				$(".pickerList").append(newht);

				if (i !== (data.length - 1)) {
					$(".pickerList").append('<div class="line"></div>');
				}
			}

			$("#showText").html('');
			for (var k = 0; k < data.length; k++) {
				var bText = '';
				var aText = '';
				if (data[k].aText) {

					aText = data[k].aText;
				}
				if (data[k].bText) {
					bText = data[k].bText;

				}
				var showtext = '<li style="width:' + offw + ';height:100%;" ><div  class="showtextContainer leftC" style="font-size:8px;" id="bText' + k + '">' + bText + '</div><div class="showtextContainer rightC" style="" id="aText' + k + '">' + aText + '</div></li>';



				$("#showText").append(showtext);



			}

			for (var j = 0; j < data.length; j++) {

				var cdata = data[j];

				var showIndex;
				var dataLength = cdata.data.length;
				var indexs = parseInt(cdata.showIndex);
				if (indexs > dataLength) {

					showIndex = cdata.data.length;
				} else if (indexs < 1) {

					showIndex = 1;
				} else {
					showIndex = cdata.showIndex;
				}

				if (cdata.type === "1") {
					this.template(cdata.data, j, showIndex);

				} else {

					this.templates(cdata.data, j, showIndex);

				}


			}
			window.localStorage['iscrollArray'] = this.iscrollArray;

		},

		iscrollArray: null,
		//组合数据数组
		getnewelement: function(hou, array) {



			var newarr = new Array();
			var inde = array.indexOf(hou);



			if (inde > 4) {


				var p = inde - 4;
				var ns = array.slice(0, p);

				var nh = array.slice(p);
				newarr = newarr.concat(nh, ns);


			} else {

				var pr = array.length - (4 - inde);

				var nw = array.slice(pr);
				var nu = array.slice(0, pr);
				newarr = newarr.concat(nw, nu);

			}



			var newelement = '';

			for (var i = 0; i < newarr.length; i++) {



				var ne = newarr[i];


				newelement = newelement + ne.outerHTML;

			}


			return newelement;

		},


		//刷新无限循环滚轴
		changeTemp: function(data, index, showIndex) {

			var selectItem = parseInt(showIndex) - 1;
			var dayListTemp = _.template($("#dataTemplate").html(), {
				"rows": data
			});


			$(".pickeritem" + index).html(" ");
			$(".pickeritem" + index).append(dayListTemp);

			var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');
			$(itemc[selectItem]).addClass('currents');



			var itemNumber;
			while (itemNumber < 9) {

				$(".pickeritem" + index).append(dayListTemp);

				itemNumber = $($(".pickeritem" + index).find('.litem')).length;

			}

			var semement = $($(".pickeritem" + index).find('.currents'))[0];


			var newelement = this.getnewelement(semement, itemc);

			$(".pickeritem" + index).html(' ');
			$(".pickeritem" + index).append(newelement);
		},

		//渲染无限循环滚轴
		template: function(data, index, showIndex) {



			var me = this;


			me.changeTemp(data, index, showIndex);


			var wrapper = "#wrapper" + index;

			var myScrolls = new IScroll(wrapper, {
				momentum: false, //禁用惯性
				bounce: false, //禁用反弹
				probeType: 3,
				mouseWheel: true
			});

			myScrolls.on('scroll', function() {
				me.updatePosition(index, myScrolls);
			});
			myScrolls.on('scrollEnd', function() {
				me.updatePositionend(index, myScrolls);
			});
			$(window).resize(function() {
				myScrolls.refresh();
			});

			myScrolls.scrollTo(0, -90);

			this.iscrollArray.push(myScrolls);


		},
		//刷新或重新渲染滚轴滚轴
		changeIscroll: function(data, index) {
			var datas = data;
			var type = data.type;
			var showindex;
			var dataLength = data.data.length;
			var indexs = parseInt(data.showIndex);
			if (indexs > dataLength) {

				showindex = data.data.length;
			} else if (indexs < 1) {

				showindex = 1;
			} else {
				showindex = data.showIndex;
			}



			if (type === "1") {
				this.changeTemp(data.data, index, showindex);

				this.iscrollArray[index].scrollTo(0, -90);
				this.iscrollArray[index].refresh();

			} else {

				this.changeTemps(data.data, index, showindex);
				var st = (showIndex - 3) * 45;
				this.iscrollArray[index].scrollTo(0, -st);
				this.iscrollArray[index].refresh();

			}
			this.getDataCallback();


		},

		//传递回调函数里面
		getDataCallback: function() {


			var data = this.getCurrentData();


			if (this.callback.iscrollupdate) {


				this.callback.iscrollupdate(this, data);
			}
		},


		//固定显示每个滚轴前文字
		renderBText: function(data, index) {

			$('#bText' + index).html('');
			$('#bText' + index).append(data);

		},
		//固定显示每个滚轴后文字
		renderAText: function(data, index) {

			$('#aText' + index).html('');
			$('#aText' + index).append(data);

		},

		//渲染title

		pickerTitle: function(data) {
			$('#pickerTitle').html('');
			$('#pickerTitle').append(data);

		},
		//获取当前所有滚轴数据
		getCurrentData: function() {

			var datas = new Array();
			var iscrolla = window.localStorage['iscrollArray'];
			for (var i = 0; i < iscrolla.length; i++) {


				var data = $($($($('#wrapper' + i).find('.pickeritem' + i)[0]).find('.currents'))[0]).attr('data');
				datas.push(data);

			}



			return datas;

		},
		//无限循环滚轴滚动结束监听
		updatePositionend: function(index, myScrolls) {

			var lastY = -90;
			var currentY = myScrolls.y;
			var cy = lastY - currentY;


			if (25 < cy && cy < 44) {

				this.scrollS(index, myScrolls);
			} else if (-44 < cy && cy < -25) {

				this.scrollXL(index, myScrolls);
			} else {
				myScrolls.scrollTo(0, -90);
			}
			// myScrolls.scrollTo(0, -90);
			console.log("h结束滑动");

			this.getDataCallback();

		},
		// 无限循环滚轴上拉时拼接新元素
		scrollS: function(index, myScrolls) {

			var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');
			var litem = itemc[0];



			var currents = $(".pickeritem" + index).find('.currents');

			$(litem).remove();


			$(".pickeritem" + index).append(litem);

			myScrolls.scrollTo(0, -90);

		},
		// 无限循环滚轴下拉时拼接新元素
		scrollXL: function(index, myScrolls) {


			var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');
			var litem = itemc[itemc.length - 1];
			var currents = $(".pickeritem" + index).find('.currents');



			$(litem).remove();
			$(".pickeritem" + index).prepend(litem);

			myScrolls.scrollTo(0, -90);

		},


		//无限循环滚轴滚动监听
		updatePosition: function(index, myScrolls) {

			var lastY = -90;
			var currentY = myScrolls.y;


			var cy = lastY - currentY;


			if (cy > 25) {
				console.log("shanglala");

				var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');
				var litem = itemc[0];

				$(itemc[4]).removeClass('currents');



				$(itemc[5]).addClass('currents');

				if (cy > 44) {

					var currents = $(".pickeritem" + index).find('.currents');

					$(litem).remove();
					$(".pickeritem" + index).append(litem);


					myScrolls.scrollTo(0, -90);
				}
			} else if (cy < -25) {




				var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');



				$(itemc[4]).removeClass('currents');

				$(itemc[3]).addClass('currents');


				if (cy < -44) {
					var litem = itemc[itemc.length - 1];
					$(litem).remove();


					$(".pickeritem" + index).prepend(litem);


					myScrolls.scrollTo(0, -90);
				}

			}


		},
		//刷新无限增加元素滚轴
		changeTemps: function(data, index, showIndex) {

			var selectItem = parseInt(showIndex) - 1;
			this.dselectIndex = selectItem;
			var dayListTemp = _.template($("#dataTemplate").html(), {
				"rows": data
			});


			$(".pickeritem" + index).html(" ");
			$(".pickeritem" + index).append(dayListTemp);
			var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');
			$(itemc[selectItem]).addClass('currents');


		},

		//渲染无限增加元素滚轴
		templates: function(data, index, showIndex) {



			var me = this;

			me.changeTemps(data, index, showIndex);
			var wrapper = "#wrapper" + index;
			var dmyScrolls = new IScroll(wrapper, {
				momentum: false, //禁用惯性
				bounce: false, //禁用反弹
				probeType: 3,
				mouseWheel: true
			});

			dmyScrolls.on('scroll', function() {
				me.dupdatePosition(index, dmyScrolls);
			});
			dmyScrolls.on('scrollEnd', function() {
				me.dupdatePositionend(index, dmyScrolls);
			});
			$(window).resize(function() {
				dmyScrolls.refresh();
			});



			this.dscrollPosition = (showIndex - 3) * 45;
			dmyScrolls.scrollTo(0, -this.dscrollPosition);


			this.iscrollArray.push(dmyScrolls);


		},
		// 无限增加元素滚轴滚动结束函数
		dupdatePositionend: function(index, dmyScrolls) {



			var lastY = -this.dscrollPosition;
			var currentY = dmyScrolls.y;
			var cy = lastY - currentY;

			if (25 < cy && cy < 44) {

				this.dscrollS(index, dmyScrolls);
			} else if (-44 < cy && cy < -25) {

				this.dscrollXL(index, dmyScrolls);
			} else {
				dmyScrolls.scrollTo(0, -this.dscrollPosition);
			}

			this.getDataCallback();
		},

		// 无限增加元素滚轴上拉时拼接新元素

		dscrollS: function(index, dmyScrolls) {
			var me = this;

			var daylength = $($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')).length;
			var litem = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')[0];


			var lastday = $($($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem'))[daylength - 1]).attr('data');
			$(litem).remove();

			var newa = me.that.addAfterEl(lastday);
			var dayListTemp = _.template($("#dataTemplate").html(), {
				"rows": newa
			});

			$(".pickeritem" + index).append(dayListTemp);



			dmyScrolls.scrollTo(0, -me.dscrollPosition);

		},
		// 无限增加滚轴下拉时拼接新元素
		dscrollXL: function(index, dmyScrolls) {

			var me = this;
			var daylength = $($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')).length;

			var litem = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')[daylength - 1];
			$(litem).remove();
			var firstdays =
				$($($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem'))[0]).attr('data');

			var newas = me.that.addBeforeEl(firstdays);


			var dayListTemps = _.template($("#dataTemplate").html(), {
				"rows": newas
			});
			$(".pickeritem" + index).prepend(dayListTemps);



			dmyScrolls.scrollTo(0, -me.dscrollPosition);

		},


		// 无限增加数据滚轴 滚动方法
		dupdatePosition: function(index, dmyScrolls) {
			var me = this;
			var lastY = -me.dscrollPosition;
			var currentY = dmyScrolls.y;



			var cy = lastY - currentY;

			var daylength = $($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')).length;
			if (cy > 25) {



				var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');


				$(itemc[me.dselectIndex]).removeClass('currents');



				$(itemc[me.dselectIndex + 1]).addClass('currents');

				if (cy > 44) {

					var litem = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')[0];


					var lastday = $($($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem'))[daylength - 1]).attr('data');
					$(litem).remove();

					var newa = me.that.addAfterEl(lastday);
					var dayListTemp = _.template($("#dataTemplate").html(), {
						"rows": newa
					});

					$(".pickeritem" + index).append(dayListTemp);



					dmyScrolls.scrollTo(0, -me.dscrollPosition);

				}

			} else if (cy < -25) {


				var itemc = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem');


				$(itemc[me.dselectIndex]).removeClass('currents');



				$(itemc[me.dselectIndex - 1]).addClass('currents');

				if (cy < -44) {

					var litem = $($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem')[daylength - 1];
					$(litem).remove();
					var firstdays =
						$($($($('#wrapper' + index).find('.pickeritem' + index)[0]).find('.litem'))[0]).attr('data');

					var newas = me.that.addBeforeEl(firstdays);


					var dayListTemps = _.template($("#dataTemplate").html(), {
						"rows": newas
					});
					$(".pickeritem" + index).prepend(dayListTemps);



					dmyScrolls.scrollTo(0, -me.dscrollPosition);

				}

			}



		},



		//点击控件外部区域取消picker显示 并返回当前数据
		pickcancleClick: function(e) {



			var el = $(e.currentTarget);
			var me = this;


			if ($(e.toElement).hasClass('displayPicker')) {

				console.info("you");

				var time = this.getCurrentData();
				if (this.callback.ok) {
					this.callback.ok(this, time);
				}
				$("#picker").hide();

			} else {

				console.info("wu");
			}



		},

		//确定按钮监听，将当前显示时间传递回调函数里面
		okClick: function() {

			// var time = this.getCurrentTime();
			// if (this.callback.ok) {


			// 	this.callback.ok(this, time);
			// }

		},
		//回调函数 ，点击外部区域回调，iscrollupdate滚轴滚动时回调

		callback: {
			ok: function(view, result) {},
			iscrollupdate: function(view, result) {},

		},
	});

	return Menu;
});