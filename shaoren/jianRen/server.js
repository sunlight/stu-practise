var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.resolve('.')));

app.get('/', function(req, res) {
	res.redirect('/app/my-index.html');
});


app.listen(process.env.PORT || 8010);
console.log('i am up.');