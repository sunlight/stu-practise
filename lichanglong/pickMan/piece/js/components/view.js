define(['zepto', 'underscore', 'backbone'], function($, _, Backbone) {

        // Cached regex to split keys for `delegate`.
        var delegateEventSplitter = /^(\S+)\s*(.*)$/;

        //handle the positon for <header> in UC & QQ browser.
        var fixHeader = function() {
            $('header').css({'width':''});
            $('header .title').css({'position':'absolute'});


            $('header').css({'position':'absolute'});


            var headerWidth = $('header').css('width');
            $('header').css({'position':'fixed'});
            $('header').css({'width':headerWidth});
        };

        var CubeView = Backbone.View.extend({
            className: 'page',
            components: {},

            //binding events for cube components
            bindEvents: function() {
                if (!this.bindings) return;

                for (var key in this.bindings) {
                    var method = this.bindings[key];
                    if (!_.isFunction(method)) method = this[method];
                    if (!method) throw new Error('Method "' + this.bindings[key] + '" does not exist');
                    var match = key.match(delegateEventSplitter);
                    var eventName = match[1],
                        componentId = match[2];
                    method = _.bind(method, this);
                    var comp = this.component(componentId);
                    if (comp) comp.on(eventName, method, this);
                }
            },
            unbindEvents: function() {
                for (var key in this.bindings) {
                    var match = key.match(delegateEventSplitter);
                    var eventName = match[1],
                        componentId = match[2];
                    var comp = this.component(componentId);
                    if (comp) comp.off(eventName);
                }
            },

            //get component instance
            component: function(id) {
                return this.components[id];
            },

            initialize: function() {
                Backbone.View.prototype.initialize.call(this);
            },

            remove: function() {
                $(window).off('orientationchange', this.onOrientationchange);
                this.unbindEvents();
                Backbone.View.prototype.remove.call(this);
            },


            render: function() {
                var self = this;
                $(document).bind('show', function() {
                    self.onShow();
                });

                this.unbindEvents();
                this.bindEvents();
                return this;
            },

            onShow: function() {
                $(window).on('orientationchange', this.onOrientationchange);
                $(window).on('resize', this.onWindowResize);
                $(window).on('scroll', this.onWindowScroll);
                $('body').css({
                    'width': '100%'
                });
                window.scroll(0, 0);
            },

            onWindowScroll: function() {
                // $('header').css({'position':'fixed'});
            },

            onOrientationchange: function() {
                $('input').blur();
            },

            onWindowResize: function() {

                //
                fixHeader();

            },

            //proxy method for page navigation
            navigate: function(fragment, options) {
                fragment = this._modularFragment(fragment);
                console.log('navigate to: ' + fragment);
                Backbone.history.navigate(fragment, options);
            },

            navigateModule: function(fragment, options) {
                Backbone.history.navigate(fragment, options);
            },

            _modularFragment: function(fragment) {
                /*modular support*/
                //this is a modular view(depends on the router), and the navigation path not start with slash,
                //we append module id in front of it
                if (this.module && fragment[0] !== '/') {
                    fragment = this.module + '/' + fragment;
                    //this is a modualr view, start with slash, means that your are using absolute path, 
                    //so we don't append module id, but remove the slash
                } else if (this.module) {
                    fragment = fragment.substring(1);
                }
                return fragment;
            }
        });

        return CubeView;
    });