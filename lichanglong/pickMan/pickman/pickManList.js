define([
		'text!pickman/pickManList.html',
		'testDrive/index',
		'shared/js/datasource',
		'listview/ListView',
		'pickman/carmarket-client'
	],
	function(viewTemplate, TestDrive, DataSource, ListView, carmarketClient) {
		return Piece.View.extend({
			id: 'carmarketList',
			events: {
				// "click .carmarket-div-out": "goToCarsDetail",
				"click .imgClick": "goToCarsDetail",
			},
			render: function() {
				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
				if (me.options.from == "tpm") {
					url = carmarketClient.path_pickman_travel_list;
					template = "travelPickMan-template";
				} else if (me.options.from == "cpm") {
					url = carmarketClient.path_pickman_eat_list;
					template = "eatPickManPickMan-template";
				}else if (me.options.from == "mpm") {
					url = carmarketClient.path_pickman_movie_list;
					template = "moviePickManPickMan-template";
				}else if (me.options.from == "carpool_cpm") {
					url = carmarketClient.path_pickman_carpool_list;
					template = "carpoolPickManPickMan-template";
				}else if (me.options.from == "rpm") {
					url = carmarketClient.path_pickman_rpm_list;
					template = "commentPickManPickMan-template";
				}


				// if (me.options.isCarsDetailReturn && me.options.currentIndex == me.options.lastIndex) {//index±È½ÏÊÇÎªÁËstateÏà»¥²»Ó°Ïì
				if (me.options.isCarsDetailReturn) {
					me._initListView(me.options.currentIndex, "session", true, url, template);
					Piece.Session.saveObject("isCarsDetailReturn", false);
				} else {
					me._initListView(me.options.currentIndex, "session", false, url, template); //´òÉÏ»º´æ±êÖ¾session
					me.listview.reloadData(); //ÖØÐÂloadÊý¾Ý£¬ÓÉÓÚ±êÖ¾sessionËùÒÔ»º´æÊý¾Ý
				}

			},
			//³õÊ¼»¯iscrollÁÐ±í
			_initListView: function(identifier, storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'carmarket-list' + identifier,
					storage: storage,
					url: url,
					pageParam: 'pageIndex',

				});


				var mainTemplate = _.template($("#carmarket-list-template").html());
				var listDiv = mainTemplate({
					id: "carmarket-list" + identifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#carmarket-list" + identifier);
				var template = _.template(this.$("#" + template).html());
				//³õÊ¼»¯´¹Ö±ÁÐ±í
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "carmarket-list" + identifier
				});
			},
			ImgChange:function(el){
                        console.log(el.toElement.height);
                         // $("#carmarket-list1").show();
                     // $(el.currentTarget).animate({"height":"96px","width":"30%"});
			},
			goToCarsDetail: function(el) {
				
                  
                 /* 方法一
                  var ID=$(el.currentTarget).parent().attr('id');
                  if (el.currentTarget.width<=$(window).width()*0.3) {
                  $(el.currentTarget).animate({"height":"auto","width":"100%"});
                  $(".content").append(el.currentTarget).attr('dataValue_id',ID);;

                 }
                 else{
                 	 var id_s=$(".content").append(el.currentTarget).attr('dataValue_id');
                 	 $(el.currentTarget).animate({"height":"96px","width":"30%"});
                 	 $("#"+id_s).append(el.currentTarget);
                 }
                   */

                   /*第二种方法

                   if ($("#imgtest").css("display")==="none") {


	                   	$("#imgtest").append("<img src="+$(el.currentTarget).attr("src")+" class='imgClick'>");



	                   	$("#imgtest").css("display","block");
                   }else{
	                   	$("#imgtest").html("");
	                   	$("#imgtest").css("display","none");
                   }
                	*/

                	// $(el.currentTarget).siblings()获得点击元素的所有兄弟节点


                	var a=$(el.currentTarget).siblings().length;//获得点击元素的兄弟节点的个数

                     // console.log($($($(el.currentTarget).siblings())[1]).attr("src")); 
                    var arr = [];
                    while (a>=0){
                    	   arr[a]=$($($(el.currentTarget).siblings())[a]).attr("src"); 
		                   a--
                    };



                   var group = $('<div>',{'id':'group','class':'slide-group'});  
					$('#imgtest').append(group);  


                  if ($("#imgtest").css("display")==="none") {
                  	   $("#imgtest").css("display","block");
                      $(".slide-group").append("<div class='slide'><img src="+$(el.currentTarget).attr("src")+" class='imgClick'></div>");
                      for (var i = 0; i <arr.length-1; i++) {
                      	if (arr[i]!="undefined") {
                        $(".slide-group").append("<div class='slide'><img src="+$($($(el.currentTarget).siblings())[i]).attr("src")+" class='imgClick'></div>");
                        }
                      }
 					  arr = [];
                   }else{
                   	   $("#imgtest").css("display","none");
	                   $(".slide-group").remove();
	                   // location.reload();
	                   
                   }
                
				// var me = this;
				// var $target = $(el.currentTarget);
				// var dataJson = $target.attr("data-json");
				// dataJson = JSON.parse(dataJson);
				// dataJson.type = me.options.from;
				// dataJson = JSON.stringify(dataJson);
				// me.listview.saveState();
				// Piece.Session.saveObject("carsDetail_dataJson", dataJson);
				// this.navigate('/carmarket/carsDetail', {
				// 	trigger: true
				// });
			},
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});