define([
		// 'text!pickman/travelPickMan.html', 'carmarket/carmarketList', 'shared/js/client', 'zepto'
		'text!pickman/carpoolPickMan.html','pickman/pickManList','shared/js/client', 'zepto'
	],
	function(viewTemplate,PickManList, Client, $) {
		return Piece.View.extend({
			id: 'travelPickMan',
			events: {
				"click .backbtn": "goBack"
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			init: function() {
				var me = this;
				//初始化tab内容
				var isCarsDetailReturn = Piece.Session.loadObject("isCarsDetailReturn");
				Piece.Session.saveObject("isCarsDetailReturn", false);
				var content = $(me.el).find(".content");
				me.travelPickMan = new PickManList({
					el: content,
					from: "carpool_cpm",
					isCarsDetailReturn: isCarsDetailReturn,
					// lastIndex: lastIndex,
					currentIndex: 1
				});
				me.travelPickMan.render();

			}

		}); //view define

	});