define([
		'text!pickman/pickManMainPage.html',
		'swipe',
		'spin',
		'shared/js/client'
	],
	function(viewTemplate, swipe, Spinner, Client) {
		return Piece.View.extend({
			events: {
				"click #travel,#eat,#movie,#carpool,#btn-home,#btn-pickan,#btn-magess,#btn-me": "goToDetail",
				// "click #btn-home":"clickImg"
				// "click #changanImpression,#passengerCars,#commercialCars,#testDrive,#cusSerOnline": "goToDetail",
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				// var me = this;
				// //保存offsetheight防止body变化导致样式改变
				// if (!Client.isNull(window.sessionStorage["carmarket-offsetwidth"]) && !Client.isNull(window.sessionStorage["carmarket-offsetheight"]) && window.sessionStorage["isGoBack"] == "true") {
				// 	me.offsetwidth = window.sessionStorage["carmarket-offsetwidth"];
				// 	me.offsetheight = window.sessionStorage["carmarket-offsetheight"];
				// } else {
				// 	window.sessionStorage["carmarket-offsetwidth"] = document.body.offsetWidth - 12;
				// 	window.sessionStorage["carmarket-offsetheight"] = document.body.offsetHeight;
				// 	me.offsetwidth = document.body.offsetWidth - 12;
				// 	me.offsetheight = document.body.offsetHeight;
				// }
				// me.initImgHeight();
				// me.initSpin();
				// me.loadImg(0);
				// me.loadImg(1);
				// me.initSwipe();
				// me.initModuleHeight();

			},
			initSpin: function() {
				var opts = {
					lines: 10, // The number of lines to draw
					length: 7, // The length of each line
					width: 3, // The line thickness
					radius: 8, // The radius of the inner circle
					color: '#ccc', // #rbg or #rrggbb
					speed: 1, // Rounds per second
					trail: 100, // Afterglow percentage
					shadow: false // Whether to render a shadow
				};
				var target0 = document.getElementById('spin0');
				var target1 = document.getElementById('spin1');
				var spinner = new Spinner(opts).spin(target0);
				var spinner = new Spinner(opts).spin(target1);
				this.$(".defauleimg").show();
			},
			initImgHeight: function() {
				var me = this;
				//<div id="spin"></div>
				me.windowwidth = window.screen.width;
				me.windowheight = window.screen.height;
				var scale = me.windowwidth / me.windowheight;
				// var scale = me.offsetwidth / me.offsetheight;
				//实现图片高度自适应，跟height=100%类似，但是设置height=100%有问题，底部modules是绝对定位
				var height;
				if (scale < 0.61) {
					height = me.offsetwidth * 175 / 353;
					me.$(".image-area").height(height);
				} else if (scale >= 0.61 && scale < 1.3) {
					height = me.offsetwidth * 120 / 314;
					me.$(".image-area").height(height);
				} else if (scale >= 1.3 && scale < 1.6) {
					height = me.offsetwidth * 240 / 1004;
					me.$(".image-area").height(height);
				} else if (scale >= 1.6) {
					height = me.offsetwidth * 170 / 1004;
					me.$(".image-area").height(height);
				}

			},
			loadImg: function(index) {
				var me = this;
				var scale = me.windowwidth / me.windowheight;
				$(".pdfimg" + index).attr("onerror", "this.src='../carmarket/img/error.ing'"); //onerror = "this.src='../carmarket/img/error.ing'";
				var im = $(".pdfimg" + index)[0];
				if (index == 0) {
					me.$(".image-area").find("#a_" + index).attr("href", "tmallchangan.html");
					if (scale < 0.61) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/passengerCars-Ad-600-353_175.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/passengerCars-Ad-800-353_175.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/passengerCars-Ad-1000-353_175.ing";
						}
					} else if (scale >= 0.61 && scale < 1.3) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/passengerCars-Ad-600-314_120.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/passengerCars-Ad-800-314_120.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/passengerCars-Ad-1000-314_120.ing";
						}
					} else if (scale >= 1.3 && scale < 1.6) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/passengerCars-Ad-600-1004_240.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/passengerCars-Ad-800-1004_240.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/passengerCars-Ad-1000-1004_240.ing";
						}
					} else if (scale >= 1.6) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/passengerCars-Ad-600-1004_170.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/passengerCars-Ad-800-1004_170.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/passengerCars-Ad-1000-1004_170.ing";
						}
					}


				} else if (index == 1) {
					me.$(".image-area").find("#a_" + index).attr("href", "tmallchana.html");
					if (scale < 0.61) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/commercialCars-Ad-600-353_175.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/commercialCars-Ad-800-353_175.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/commercialCars-Ad-1000-353_175.ing";
						}
					} else if (scale >= 0.61 && scale < 1.3) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/commercialCars-Ad-600-314_120.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/commercialCars-Ad-800-314_120.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/commercialCars-Ad-1000-314_120.ing";
						}
					} else if (scale >= 1.3 && scale < 1.6) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/commercialCars-Ad-600-1004_240.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/commercialCars-Ad-800-1004_240.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/commercialCars-Ad-1000-1004_240.ing";
						}
					} else if (scale >= 1.6) {
						if (me.windowheight < 540) {
							im.src = "../carmarket/img/commercialCars-Ad-600-1004_170.ing";
						} else if (me.windowheight >= 540 && me.windowheight <= 854) {
							im.src = "../carmarket/img/commercialCars-Ad-800-1004_170.ing";
						} else if (me.windowheight > 854) {
							im.src = "../carmarket/img/commercialCars-Ad-1000-1004_170.ing";
						}
					}
				}
				im.onload = function() {
					me.$(".defauleimg" + index).hide();
				};
			},

			//初始化模块容器高度
			initModuleHeight: function() {
				var me = this;
				var top = me.$("#adArea").height();
				// me.$(".carsMainPage-modules").css("top", top);
				me.$(".carsMainPage-modules").height(me.offsetheight - me.$("#adArea").height() - 45 - 45 - 6);
				// alert(me.$(".carsMainPage-modules").height());
				// var height = me.$(".line_one_left").height();
				var height;
				if (!Client.isNull(window.sessionStorage["carmarket-height"]) && window.sessionStorage["isGoBack"] == "true") {
					height = window.sessionStorage["carmarket-height"];
					window.sessionStorage["isGoBack"] = false;
				} else {
					window.sessionStorage["carmarket-height"] = me.$(".line_one").height();
					// height = me.$(".line_one_left").height();
					height = me.$(".line_one").height();
				}
				// var width = 234 / 352 * height;
				var width = 234 / 352 * height;
				me.$(".line_one_left").width(width);
				me.$(".line_one").height(height); //防止手机body变化导致样式改变
				me.$(".line_one_right").height(height);
				me.$(".line_one_right").css("left", width + 6);
				me.$(".line_one_right>div:nth-child(1)").height(height / 2 - 3);
				me.$(".line_one_right>div:nth-child(2)").height(height / 2 - 3);
				me.$(".line_two_left").width(me.$(".line_two_left").width() - 3);
				me.$(".line_two_right").width(me.$(".line_two_right").width() - 3);
				me.$(".line_two").height(me.$(".line_two").height());
				//表格table做法，TODO
				/*me.$(".table-module").height(me.offsetheight-me.$("#adArea").height()-45-48);
				me.$("#changanImpression").width(width);*/
				if (me.offsetheight <= 356) {
					me.$(".carsMainPage-modules").css("font-size", "14px");
					me.$("#changanImpression").css("margin-left", "-28px");
				}

			},
			//初始化swipe
			initSwipe: function() {
				var me = this;
				me.$("#position").css("display", "block");
				var elem = document.getElementById('adSwipe');
				me.mySwipe = new Swipe(elem, {
					startSlide: 0,
					speed: 500,
					// auto: 4000,
					continuous: true,
					disableScroll: false,
					stopPropagation: true,
					callback: function(index, elem) {
						var whichPage;
						index = isNaN(index) ? 0 : index;

						if ($("#position").children("li").length == 2) {
							index = index % 2;
						}

						whichPage = index + 1;
						// me.loadImg(index);
						$("#position").children("li").removeClass("on");
						$("#position").children("li:nth-child(" + whichPage + ")").addClass("on");
						me.mySwipe.restart();
					},
					transitionEnd: function(index, elem) {}
				});

			},
			changeImg:function(el) {
		          var src=(el.toElement.src).replace("")
		          var b=src.substring(src.length-6)  //取出文件名
		          // console.log(b);
		          if (b==="up.png") {
		          	$("#btn-home").attr("src","../img/mainImg/bottom_check0_up.png");
		          	$("#btn-pickan").attr("src","../img/mainImg/bottom_check1_up.png");
		          	$("#btn-magess").attr("src","../img/mainImg/bottom_check2_up.png");
		          	$("#btn-me").attr("src","../img/mainImg/bottom_check3_up.png");

		          	var f=src.substring(src.length-20);//取出图片的名字
		          	var g=f.substring(0,14)+"down.png";
		            var id=el.toElement.id;
		          	$("#"+id).attr("src","../img/mainImg/"+g);
		          	
                    }

		          
   			 },
			goToDetail: function(el) {
				// console.log(el.toElement.id)	;
				
				var me = this;
				
				Piece.Session.saveObject("isGoBack", true);
				var $target = $(el.currentTarget);
				var dataValue = $target.attr("data-value");

				/*dataJson = JSON.parse(dataJson);
				dataJson.type = me.options.from;
				dataJson = JSON.stringify(dataJson);*/
				// Piece.Session.saveObject("carsDetail_dataJson", dataJson);
				if (dataValue == "travelPickMan") {
					console.log(dataValue)
					this.navigate('/pickman/travelPickMan', {
						trigger: true
					});
				} else if (dataValue == "eatPickMan") {
						console.log(dataValue)
					this.navigate('/pickman/eatPickMan', {
						trigger: true
					});
				} else if (dataValue == "moviePickMan") {
						console.log(dataValue)
					this.navigate('/pickman/moviePickMan', {
						trigger: true
					});
				} else if (dataValue == "carpoolPickMan") {
						console.log(dataValue)
					this.navigate('/pickman/carpoolPickMan', {
						trigger: true
					});
				} else if(dataValue=="btn-home"){
					me.changeImg(el);

				}else if(dataValue=="btn-pickan"){
					me.changeImg(el);

				}else if(dataValue=="btn-magess"){
					me.changeImg(el);

				}else if(dataValue=="btn-me"){
					me.changeImg(el);

				}




			}

		}); //view define

	});