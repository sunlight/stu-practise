define([], function() {
	return {
		path_pickman_travel_list: "../tpm/data.json",
		path_pickman_eat_list:"../cpm/data.json",
		path_pickman_movie_list: "../mpm/data.json",
		path_pickman_carpool_list: "../carpool_cpm/data.json",
		path_pickman_rpm_list: "../rpm/data.json"
	};
});