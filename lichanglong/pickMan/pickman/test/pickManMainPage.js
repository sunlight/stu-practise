define([
		'text!pickman/pickManMainPage.html'
	],
	function(viewTemplate) {
		return Piece.View.extend({
			id: 'body',
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			}
	

				}); //view define

	});