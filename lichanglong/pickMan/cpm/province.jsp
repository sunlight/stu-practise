<%@page import="org.json.*"%>
<%@page import="com.ca.meap.contact.om.DepartmentInfo"%>
<%@page import="com.ca.meap.contact.om.UserContactInfo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="com.ca.meap.test.*" import="org.apache.http.client.*"
	import="org.apache.http.*" import="org.apache.http.impl.client.*"
	import="com.ca.meap.app.context.*" import="java.io.*"
	import="java.lang.*"
	import="com.cait.workflowclient.Configuration.AppOption"
	import="com.ca.meap.framework.om.*"
	import="com.ca.meap.framework.service.*" import="com.ca.meap.mdm.bll.*"
	import="com.cait.framework.data.*" import="com.cait.framework.utils.*"
	import="com.ca.meap.adapter.service.*" import="java.util.*"
	import="java.sql.*"
%>



<%
    Connection con = null;// 创建一个数据库连接
    PreparedStatement pre = null;// 创建预编译语句对象，一般都是用这个而不用Statement
    ResultSet result = null;// 创建一个结果集对象
    JSONObject value = new JSONObject();
    JSONArray list = new JSONArray();
    try {
		Class.forName("oracle.jdbc.driver.OracleDriver");// 加载Oracle驱动程序
		System.out.println("开始尝试连接数据库！");
		String url = "jdbc:oracle:" + "thin:@101.227.252.199:1521:orcl";// 127.0.0.1是本机地址，XE是精简版Oracle的默认数据库名
		String user = "SDCMSDBACA";// 用户名,系统默认的账户名
		String password = "sdh79gwhup";// 你安装时选设置的密码
		con = DriverManager.getConnection(url, user, password);// 获取连接
		//System.out.println("连接成功！");
		String sql = "select * from changan_auto_area t where t.levels='1'";// 预编译语句，“？”代表参数
		pre = con.prepareStatement(sql);// 实例化预编译语句

		result = pre.executeQuery();// 执行查询，注意括号中不需要再加参数

		while (result.next()) {
		    JSONObject v = new JSONObject();
		    v.put("id", String.valueOf(result.getInt("ID")));
		    v.put("name", result.getString("AREA_NAME"));
		    list.add(v);
		}
		value.put("data", list);

    } catch (Exception e) {
		e.printStackTrace();
    } finally {
		try {
		    // 逐一将上面的几个对象关闭，因为不关闭的话会影响性能、并且占用资源
		    // 注意关闭的顺序，最后使用的最先关闭
		    if (result != null)
			result.close();
		    if (pre != null)
			pre.close();
		    if (con != null)
			con.close();
		    //System.out.println("数据库连接已关闭！");
		} catch (Exception e) {
		    e.printStackTrace();
		}
    }
    out.write(value.toString());
%>