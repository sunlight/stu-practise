define(['text!testDrive/index.html',
				'zepto',
				'../shared/picker/picker',
				'moment',
				'shared/js/notification',
				'testDrive/testDrive_client',
				'css!testDrive/index'],function(viewTemplate, $, Picker, Moment, Notification, Client){
	return Piece.View.extend({
		id:'testDrive_index',
		displayId:'testDrive_index',
		displayName:'预约试驾',
		events:{
			'click .time': 'onSelectPicker',
			'click .carStyle': 'onToggleSetting',
			'click .footer': 'onConfirm',
			
		},
		render:function() {
			$(this.el).html(viewTemplate);
			Piece.View.prototype.render.call(this);
			return this;
		},
		remove:function() {
			Piece.View.prototype.remove.call(this);
		},
		onShow:function(){ 
			// this.initScroller();
			this.uiSetWidth();
			this.initTimePicker();
			this.uiSetTextareaMinHeight();
			this.uiSetTextareaHeight();
			this.uiSetCarTypeList("pv");
			this.uiBindChange();
			// this.onWindowResize();
			
		},
		uiSetWidth:function() {
			var  width= this.$el.find(".information-content").width();
			this.$el.find(".calc-width").width(width - 16);		//设置input的宽度
		},
		
		initTimePicker:function(){
			this.picker = new Picker(this.$el, this);
      this.picker.callback = this;
		},
		uiSetTextareaMinHeight:function() {		//设置补充的最小高度
			var screenHeight = $(window).height() - 44;
			var personInfoHeight = 142;
			var typeHeight = 52;
			var testDriveInformationHeight = 106;
			var timeHeight = 42;
			var footerHeight = 42 + 10 + 23;
			var agendaContentMinHeight = screenHeight - personInfoHeight - typeHeight 
																	- testDriveInformationHeight - timeHeight  - footerHeight;
			agendaContentMinHeight = agendaContentMinHeight > 130 ? agendaContentMinHeight :130;														
			this.$el.find('.additional-remarks').find(".text").css("min-height",agendaContentMinHeight);
		},
		uiSetTextareaHeight:function() {
			var me = this;
			var agendaEl = me.$el.find('.additional-remarks').find(".text");
			var oldScrollHeight = agendaEl[0].scrollHeight;
			agendaEl.height(oldScrollHeight + 2);
			// me.testDriveScroller.refresh();
			
			me.$el.find('.additional-remarks').find(".text").bind('input prototypechange', function() {
				var textareaHeight = me.$el.find('.additional-remarks').find(".text").height();
				var textareaScrollHeight = me.$el.find('.additional-remarks').find(".text")[0].scrollHeight;
				if (textareaHeight < textareaScrollHeight) {
					me.$el.find('.additional-remarks').find(".text").height(textareaScrollHeight + 2);
					// me.testDriveScroller.refresh();
				}
			});
		},
		uiSetCarTypeList:function(type) {
			var me = this;
			var carTypeSelect = me.$el.find(".type");
			carTypeSelect.parent().find(".icon-close").css("display","none");
			carTypeSelect.parent().find(".icon-check").css("display","none");
			Client.getCarStyleList({
						type: type,
						beforeSend: function() {
							if(me.$el.parent().find('.chrysanthemum').length===0){
								me.$el.parent().append("<div class='chrysanthemum active'><div></div></div>");
								me.$el.find(".mask").show();
							}	
						},
						success: function(data) {
							if(data.data) {
								carTypeSelect.children().remove();
								carTypeSelect.append("<option value='0'>请选择您要试用的车型</option>");
								console.log(data);
								_.each(data.data, function(dataItem){
									var carTypeItem = "<option value='" + dataItem.id + "'>" +  dataItem.name + "</option>";
									carTypeSelect.append(carTypeItem);
								});
							} else {
								carTypeSelect.children().remove();
								carTypeSelect.append("<option value='0'>请选择您要试用的车型</option>");
							}
						},
						error: function(error) {
							carTypeSelect.children().remove();
							carTypeSelect.append("<option value='0'>请选择您要试用的车型</option>");
						},
						complete: function() {
							me.$el.parent().find('.chrysanthemum').remove();
							me.$el.find(".mask").hide();
						}
					});
		},
		uiSetProvinceList:function() {
			var me = this;
			/*var proviceArr = ["北京","上海","天津","重庆","河北省","山西省","内蒙古自治区","辽宁省","吉林省","黑龙江省"
			,"江苏省","浙江省","安徽省","福建省","江西省","山东省","河南省","湖北省","湖南省","广东省","广西壮族自治区",
			"海南省","四川省","贵州省","云南省","西藏自治区","陕西省","甘肃省","宁夏回族自治区","青海省","新疆维吾尔族自治区",
			"香港特别行政区","澳门特别行政区","台湾省"];
			var proviceSelect = me.$el.find(".province");
			for(var i = 0; i < proviceArr.length; i++) {
				var provinceItem = "<option value='" + (i+1) + "'>" + proviceArr[i] + "</option>";
				proviceSelect.append(provinceItem);
			}
			$(proviceSelect).change(function(){
				var value = $(proviceSelect).val()
				me.uiSetCityList(value);
			});*/
			var me = this;
			var proviceSelect = me.$el.find(".province");
			var type = me.$el.find(".check").attr("data-value");
			Client.getProvinceList({
						type: type,
						beforeSend: function() {
							if(me.$el.parent().find('.chrysanthemum').length===0){
								me.$el.parent().append("<div class='chrysanthemum active'><div></div></div>");
								me.$el.find(".mask").show();
							}	
						},
						success: function(data) {
							if(data.data) {
								proviceSelect.children().remove();
								proviceSelect.append("<option value='0'>请选择省份</option>");
								console.log(data);
								_.each(data.data, function(dataItem){
									var provinceItem = "<option value='" + dataItem.id + "'>" +  dataItem.name + "</option>";
									proviceSelect.append(provinceItem);
								});
							} else {
								proviceSelect.children().remove();
								proviceSelect.append("<option value='0'>请选择省份</option>");
							}
						},
						error: function(error) {
							proviceSelect.children().remove();
							proviceSelect.append("<option value='0'>请选择省份</option>");
						},
						complete: function() {
							me.$el.parent().find('.chrysanthemum').remove();
							me.$el.find(".mask").hide();
						}
					});
		},
		uiSetCityList:function(id) {
			var me = this;
			var citySelect = me.$el.find(".city");
			var type = me.$el.find(".check").attr("data-value");
			Client.getCityList({
				type: type,
				id:id,
				beforeSend: function() {
					if(me.$el.parent().find('.chrysanthemum').length===0){
						me.$el.parent().append("<div class='chrysanthemum active'><div></div></div>");
						me.$el.find(".mask").show();
					}
				},	
				success:function(data){
					console.log(data);
					console.log("数据")
					if(data.data) {
						citySelect.children().remove();
						citySelect.append("<option value='0'>请选择城市</option>");
						console.log(data);
						_.each(data.data, function(dataItem){
							var cityItem = "<option value='" + dataItem.id + "'>" +  dataItem.name + "</option>";
							citySelect.append(cityItem);
						});
					} else {
						citySelect.children().remove();
						citySelect.append("<option value='0'>请选择城市</option>");
					}
				},
				error:function(error){
					citySelect.children().remove();
					citySelect.append("<option value='0'>请选择省份</option>");
				},
				complete:function(){
					me.$el.parent().find('.chrysanthemum').remove();
					me.$el.find(".mask").hide();
				}
			});
		},
		uiSetDistributorList:function(id) {
			var me = this;
			var distributorSelect = this.$el.find(".distributor-name");
			var type = this.$el.find(".check").attr("data-value");
			Client.getDealerList({
				type: type,
				id: id,
				beforeSend: function() {
					if(me.$el.parent().find('.chrysanthemum').length===0){
						me.$el.parent().append("<div class='chrysanthemum active'><div></div></div>");
						me.$el.find(".mask").show();
					}
				},
				success:function(data){
					if(data.data && data.data.length > 0) {
						distributorSelect.children().remove();
						distributorSelect.append("<option value='0'>请选择经销商</option>");
						console.log(data);
						_.each(data.data, function(dataItem){
							var distributorItem = "<option value='" + dataItem.id + "'>" +  dataItem.name + "</option>";
							distributorSelect.append(distributorItem);
						});
					} else {
						distributorSelect.children().remove();
						distributorSelect.append("<option value='0'>请选择经销商</option>");
						distributorSelect.append("<option value='00'>暂无该地区经销商</option>");
					}
				},
				error:function(){
					distributorSelect.children().remove();
					distributorSelect.append("<option value='0'>请选择经销商</option>");
					distributorSelect.append("<option value='00'>暂无该地区经销商</option>");
				},
				complete:function(){
					me.$el.parent().find('.chrysanthemum').remove();
					me.$el.find(".mask").hide();
				}
			});

		},
		uiBindChange:function() {		//绑定改变事件
			var me = this;
			var nameEl = me.$el.find(".name");
			nameEl.find("input").change(function(){
				if(nameEl.find("input").val().length > 0) {
					nameEl.find(".icon-check").css("display","inline");
					nameEl.find(".icon-close").css("display","none");
				} else {
					nameEl.find(".icon-check").css("display","none");
					nameEl.find(".icon-close").css("display","inline");
				}
			});

			var phoneEl = me.$el.find(".phonecall");
			phoneEl.find("input").change(function(){
				if(phoneEl.find("input").val().length == 11) {
					phoneEl.find(".icon-check").css("display","inline");
					phoneEl.find(".icon-close").css("display","none");
				} else {
					phoneEl.find(".icon-check").css("display","none");
					phoneEl.find(".icon-close").css("display","inline");
				}
			});

			var emailEl = me.$el.find(".email");
			emailEl.find("input").change(function(){
				var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
				if(filter.test(emailEl.find("input").val())) {
					emailEl.find(".icon-check").css("display","inline");
					emailEl.find(".icon-close").css("display","none");
				} else {
					emailEl.find(".icon-check").css("display","none");
					emailEl.find(".icon-close").css("display","inline");
				}
			});

			var typeEl = me.$el.find(".type");
			typeEl.change(function(){
				provinceEl.children().remove();
				provinceEl.append("<option value='0'>请选择省份</option>");
				provinceEl.parent().find(".icon-check").css("display","none");
				provinceEl.parent().find(".icon-close").css("display","none");
				cityEl.children().remove();
				cityEl.append("<option value='0'>请选择城市</option>");
				cityEl.parent().find(".icon-check").css("display","none");
				cityEl.parent().find(".icon-close").css("display","none");
				distributorEl.children().remove();
				distributorEl.append("<option value='0'>请选择经销商</option>");
				distributorEl.parent().find(".icon-close").css("display","none");
				distributorEl.parent().find(".icon-check").css("display","none");
				if(typeEl.val() > 0) {
					typeEl.parent().find(".icon-check").css("display","inline");
					typeEl.parent().find(".icon-close").css("display","none");
					me.uiSetProvinceList();
				} else {
					typeEl.parent().find(".icon-check").css("display","none");
					typeEl.parent().find(".icon-close").css("display","inline");
				}
			});

			var provinceEl = me.$el.find(".province");
			provinceEl.change(function(){
				cityEl.children().remove();
				cityEl.append("<option value='0'>请选择城市</option>");
				distributorEl.children().remove();
				distributorEl.append("<option value='0'>请选择经销商</option>");
				cityEl.parent().find(".icon-check").css("display","none");
				cityEl.parent().find(".icon-close").css("display","none");
				distributorEl.parent().find(".icon-close").css("display","none");
				distributorEl.parent().find(".icon-check").css("display","none");
				var provinceId = provinceEl.val();
				if(provinceId > 0) {
					me.uiSetCityList(provinceId);
					provinceEl.parent().find(".icon-check").css("display","inline");
					provinceEl.parent().find(".icon-close").css("display","none");
				} else {
					provinceEl.parent().find(".icon-check").css("display","none");
					provinceEl.parent().find(".icon-close").css("display","inline");
				}
			});

			var cityEl = me.$el.find(".city");
			cityEl.change(function(){
				distributorEl.children().remove();
				distributorEl.append("<option value='0'>请选择经销商</option>");
				var cityId = cityEl.val();
				if(cityId > 0) {
					cityEl.parent().find(".icon-check").css("display","inline");
					cityEl.parent().find(".icon-close").css("display","none");
					me.uiSetDistributorList(cityId);
				} else {
					cityEl.parent().find(".icon-check").css("display","none");
					cityEl.parent().find(".icon-close").css("display","inline");
				}
			});

			var distributorEl = me.$el.find(".distributor-name");
			distributorEl.change(function(){
				if(distributorEl.val() > 0) {
					distributorEl.parent().find(".icon-check").css("display","inline");
					distributorEl.parent().find(".icon-close").css("display","none");
				} else {
					distributorEl.parent().find(".icon-check").css("display","none");
					distributorEl.parent().find(".icon-close").css("display","inline");
				}
			});
		},
		addAfterEl: function(oldLastYear) {			//向上滑的时候的拼接数据
        console.log(oldLastYear);
        console.log("addAfterEl");
        var appendYearList = [];
        var appendYear = 0;
        for(var i = 1; i <= 1; i++) {
        	appendYear = parseInt(oldLastYear) + i;
        	appendYearList.push(appendYear)
        }

        return appendYearList;
      },
      addBeforeEl: function(oldLastYear) {		//向下滑的时候的拼接数据

        console.log(oldLastYear);
        console.log("addBeforeEl");
        var appendYearList = [];
        var appendYear = 0;
        for(var i = 1; i >= 1; i--) {
        	appendYear = parseInt(oldLastYear) - i;
        	appendYearList.push(appendYear);
        }

        return appendYearList;

      },
		getYearList:function(initYear){		//获得前后五年的数据
			var yearList = [];
			var year = parseInt(initYear) - 5;
			for(var i = 0; i <= 10; i++) {
				yearList.push(year);
				year = year + 1;
			}
			return yearList;
		}, 
		getDayList:function(year, month){			//获得该月的日期
			var dayList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31"];
			var days = new Date(year, month, 0).getDate(); 
			dayList.length = days;
			return dayList;
		},
		iscrollupdate:function(view, result){
			console.log(result);
			this.oldMonth = this.oldMonth == undefined ? result[1] : this.oldMonth;
			if(result[1] != this.oldMonth) {
				var dayList = this.getDayList(result[0], result[1]);
				var dayIndex = result[2];
				var dayData = {
					"type": "1",
          "data": dayList,
          "showIndex": dayIndex,
          "bText": '',
          "aText": "日"
				}
				 this.oldMonth = result[1]; 
				this.picker.changeIscroll(dayData,2);
			}
			$("#pickerTitle").html(result[0] + "年" + result[1] + "月" + result[2] + "日");
		},
		onSelectPicker:function(e) {
			var timeText = this.$el.find(".time").text();
			if(timeText && timeText.length > 0) {
				var date = moment(timeText)._d;
			} else {
				var date = moment()._d;
			}
			
			var year = date.getFullYear();
			var month = date.getMonth() + 1;
			var day = date.getDate();
			var yearList = this.getYearList(year);
			var monthList = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
			var dayList = this.getDayList(year,month);
			var yearIndex = "6";
			var monthIndex = month;
			this.oldMonth = month;
			var dayIndex = day;
			var data = [{
          "type": "2",
          "data": yearList,
          "showIndex": yearIndex,
          "bText": '',
          "aText": "年"
        }, {
          "type": "1",
          "data": monthList,
          "showIndex": monthIndex,
          "bText": '',
          "aText": "月"
        }, {
          "type": "1",
          "data": dayList,
          "showIndex": dayIndex,
          "bText": '',
          "aText": "日"
        }

      ];
			this.timeEl = $(e.currentTarget);

			this.picker.showPicker(data);
			$("#pickerTitle").html(year + "年" + month + "月" + day + "日");
		},
		ok: function(view, result) {

			var resultTime = new Date(result[0], result[1] - 1, result[2]);
			if(moment(resultTime)._d.getTime()>=moment()._d.getTime() || moment(resultTime)._d.getDate() == moment()._d.getDate()){
			    this.timeEl.text(moment(resultTime).format("YYYY-MM-DD"));
			}else{
				Notification.show({
					type: "error",
					message: "预约时间不能小于当前时间"
				});
			}
			if(this.timeEl.text().length > 0) {
				this.timeEl.parent().find(".icon-check").css("display","inline");
			} else {
				this.timeEl.parent().find(".icon-check").css("display","none");
			}
		},

		onConfirm:function() {		//确定
			var me = this;
			var checkList = me.$el.find(".icon-check");
			var index = 0;
			_.each(checkList, function(checkItem) {
				if($(checkItem).css("display") == "inline") {
					index++;
				} 
			});
			if(index == checkList.length) {
				me.saveTestDrive();
			} else {
				Notification.show({
					type: "error",
					message: "请完善必填信息"
				});
			}
		},
		saveTestDrive:function(){			//预约试驾
			var me = this;
			var type = me.$el.find(".check").attr("data-value");
			var nameEl = me.$el.find(".name");
			var name = nameEl.children("input").val();	//姓名
			var phoneEl = me.$el.find(".phonecall");
			var phonecall = phoneEl.children("input").val();	//手机
			var emailEl = me.$el.find(".email"); 
			var email = emailEl.children("input").val();			//邮件
			var typeEl = me.$el.find(".type");
			var style = typeEl.val();			//试用车型
			var provinceId = me.$el.find(".province").val();	//省份
			var cityId = me.$el.find(".city").val();			//城市
			var dealerId = me.$el.find(".distributor-name").val();	//经销商
			var time = me.$el.find(".time").text();			//预约时间
			var additionalRemarks = me.$el.find(".additional-remarks").children(".text").val();		//补充说明
			Client.testDrive({
				"type": type,
				"provinceId": provinceId,
				"cityId": cityId,
				"dealerId": dealerId,
				"carId": style,
				"name": name,
				"mobile": phonecall,
				"email": email,
				"date":time,
				"txt": additionalRemarks,
				beforeSend:function(){
					if(me.$el.parent().find('.chrysanthemum').length===0){
						me.$el.parent().append("<div class='chrysanthemum active'><div></div></div>");
						me.$el.find(".mask").show();
					}
				},
				success:function(data){
					if(data.success) {
						Notification.show({
							type: "well",
							message: "预约成功"
						});
					} else {
						Notification.show({
							type: "error",
							message: "预约失败"
						});
					}

				},
				error:function(){
					Notification.show({
						type: "error",
						message: "预约失败"
					});
				},
				complete:function() {
					me.$el.parent().find('.chrysanthemum').remove();
					me.$el.find(".mask").hide();
				}
			});
		},
		 refresh:function(){
				var me =this;
				setTimeout(function(){
					// me.testDriveScroller.refresh();
				},100)
			},
			onToggleSetting:function(e) {
				var target = e.currentTarget;
				var oldValue = this.$el.find('.check').attr("data-value");
				this.$el.find('.setting-icon').removeClass('check');
				$(target).find('.setting-icon').addClass('check');
				var newValue = this.$el.find('.check').attr("data-value");
				if(oldValue != newValue) {
					// this.$el.find(".icon-check").hide();
					var provinceEl = this.$el.find(".province");
					var cityEl = this.$el.find(".city");
					var distributorEl = this.$el.find(".distributor-name")
					provinceEl.children().remove();
					provinceEl.append("<option value='0'>请选择省份</option>");
					provinceEl.parent().find(".icon-check").css("display","none");
					provinceEl.parent().find(".icon-close").css("display","none");
					cityEl.children().remove();
					cityEl.append("<option value='0'>请选择城市</option>");
					cityEl.parent().find(".icon-check").css("display","none");
					cityEl.parent().find(".icon-close").css("display","none");
					distributorEl.children().remove();
					distributorEl.append("<option value='0'>请选择经销商</option>");
					distributorEl.parent().find(".icon-close").css("display","none");
					distributorEl.parent().find(".icon-check").css("display","none");
					this.uiSetCarTypeList(newValue);
				}
			},
			
	});
});