define([
	'text!pickman/pick2.html',
	'shared/js/datasource',
	'listview/ListView',
	'shared/js/client',
	'shared/js/swiper.min'
	],function(viewTemplate,DataSource,ListView,Client,Swiper){
		return Piece.View.extend({
			//装列表的div
			id:"pick2-list",
			imgArrays : new Array(),
			toType : "",
			events : {
				"click .btn_back" : "goToback",
				"click .text" : "showText",
				//"touchstart .one_img" : "viewImgs",
				"touchstart #one_img1,#one_img2,#one_img3" : "viewImgs",
				"click .swiper-slide" : "disableImg",//隐藏放大的图片
				"click .review" : "goToDetail"
			},
			render : function(){
				//得到参数
				this.toType = Client.request("dataValue");				
				$(this.el).html(viewTemplate);
				console.info("el=="+this.el);
				Piece.View.prototype.render.call(this);	
				return this;
			},			
			onShow : function(){			
				var me = this;
				var template = "";
				var url = "";				
				// url = "../datajson/travel.json";
                url = this.findDataJson(this.toType);
				//url = "../datajson/"+toType+".json";
				//缓存session，浏览的位置
    //             var isPick2DetailReturn = Piece.Session.loadObject("isPick2DetailReturn");
				// Piece.Session.saveObject("isPick2DetailReturn", false);

				template = "pick2_all";
				me._initListView(1,"session",false,url,template);
				me.listview.reloadData();		
			},
			findDataJson :function(type){
				//有travel  eat   movie   carpool
                if(type=="travel") {
                	$("#pick2_head").text("一起旅游");
                	return "../datajson/travel.json";
                }else if (type=="eat") {
                	$("#pick2_head").text("美食天下");
                    return "../datajson/eat.json";
                }else if (type=="movie") {
                	$("#pick2_head").text("无限影院");
                	return "../datajson/movie.json";
                }else {
                	$("#pick2_head").text("快乐自驾");
                	return "../datajson/carpool.json";
                };
			},
			goToback :function(){
				window.history.back(-1);
			},
			_initListView : function(indentifier, storage, autoLoad, url, template){
				var me = this;
				this.datasource = new DataSource({
					indentifier : 'pick2-list' + indentifier,
					storage : storage,
					url : url,
					pageParam : 'pageIndex'
				});

				var mainTemplate = _.template($("#pick2-list-template").html());
				var listDiv = mainTemplate({
					id : "pick2-list" + indentifier
				});
				$(".content").append(listDiv);
				var listEl = this.el.querySelector("#pick2-list"+indentifier);
				var template = _.template(this.$("#" + template).html());	
				//初始化垂直列表
				this.listview = new ListView({
					el : listEl,
					itemTemplate : template,
					dataSource : this.datasource,
					autoLoad : autoLoad,
					id : "pick2-list" + indentifier
				});
			},			
			showText : function(event){
				var text = $(event.currentTarget);
				if (text.css("display") === "-webkit-box") {
					text.css({"display":"inline-block"});
				}else{
                    text.css({"display":"-webkit-box"});
				}
			},
			goToDetail : function(el){
				
				var me = this;
				//获得当前单击目标
				var $target = $(el.currentTarget);
				var datas = $target.attr("data-json");
				datas = JSON.parse(datas);
				//存储datas
				Piece.Session.saveObject("reviewIdAndCount",datas);
				this.navigate('/pickman/review', {
						trigger: true
					});
			},
			viewImgs : function(el){
				//初始化swipe插件				
				var clickImg = $(el.currentTarget);
				this.imgArrays = this.getLevelImg(clickImg);	
				//$(".content").hide();
				$(".bar").hide();

                //创建装图片的div
				var div = document.createElement("div");
				div.className = "swiper-wrapper";
				div.id = "swiper-wrapper";
				document.getElementById("swiper-container").appendChild(div);
                //创建切换点
				var div1 = document.createElement("div");
				div1.className = "swiper-pagination";
				document.getElementById("swiper-container").appendChild(div1);

				$(".swiper-wrapper").animate({"width":"100%","height":"100%","top":"0px"},"slow");

				 this.createImg(div,clickImg.attr("src"),$(el.currentTarget));//创建点击的图片
				
				// 

			},
			disableImg : function(){
				var slide = document.getElementById("swiper-container").getElementsByTagName("div");
				// console.info(slide.length);
				if(slide){		
					$(".swiper-wrapper").animate({"width":"20%","height":"0%","top":"20px"},"slow");
					$(".swiper-slide").animate({"width":"0%","height":"0%","top":"0px","left":"0"},"slow");
					document.getElementById("swiper-container").innerHTML = "";
					
					$(".content").show();
					$(".bar").show(); 
				}
			},
			getLevelImg : function (nowNode){
				return  nowNode.parent().children("img");
			},
			createImg : function(parent,src,click_img){//创建图片
				var $clickImg = click_img;

				var divSlide = document.createElement("div");
				divSlide.className = "swiper-slide";
				divSlide.style.width = "0";
				divSlide.style.height = "100%";
				divSlide.style.top = "0";
				divSlide.style.right = "0";
				
				parent.appendChild(divSlide);

				var img = document.createElement("img");
				img.src = src;
				divSlide.appendChild(img);
				//图片宽高
				img.style.width = "100%";				
				// var imgx = img.width;
				var imgy = img.height;
				var Y = document.body.clientHeight;
				 img.style.top = Y-imgy/2;				
				$(".swiper-slide").animate({"width":"100%","height":"100%","top":"0px","right":"0"},"slow");
			}
		});
});