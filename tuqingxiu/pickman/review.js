define([
	'text!pickman/review.html',
	'shared/js/datasource',
	'listview/ListView',
	'shared/js/client',
	'shared/js/swiper.min'
	],function(viewTemplate,DataSource,ListView,Client,Swiper){
		return Piece.View.extend({
			//装列表的div
			id:"review-list",
			imgArrays : new Array(),
			// reviewCount: '',
			events : {
				"click .btn_back" : "goToback",
			},
			render : function(){		
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);	
				return this;
			},			
			onShow : function(){			
				var me = this;
				var template = "";
				var url = "";	
				var reviewId = Piece.Session.loadObject("reviewIdAndCount");			
				url = "../datajson/"+reviewId.id+".json";
                // this.addReviewCount(reviewId.reviews);
   

				template = "review_all";
				me._initListView(1,"session",false,url,template,reviewId.reviews);
				me.listview.reloadData();		
			},
			addReviewCount :function(count){
				var me = this;
				this.reviewCount = document.createElement("div");
				this.reviewCount.className = "reviewCount";
				this.reviewCount.text = "评论  "+count;
				// me.$el.find("#reviewCount").append(count);
				// $("#reviewCount").text("pinglun"+count);
			},
			goToback :function(){
				window.history.back(-1);
			},
			_initListView : function(indentifier, storage, autoLoad, url, template,count){
				var me = this;
				this.datasource = new DataSource({
					indentifier : 'review-list' + indentifier,
					storage : storage,
					url : url,
					pageParam : 'pageIndex'
				});

				var mainTemplate = _.template($("#review-list-template").html());
				var listDiv = mainTemplate({
					id : "review-list" + indentifier
				});
				//将列表内容添加到content中prepend
				var reviewCount = document.createElement("div");
				reviewCount.className = "reviewCount";								
				$(".content").append(listDiv);
				$(".carmarket-scroller").prepend(reviewCount);
				$(".reviewCount").text("评论 "+count);
				var listEl = this.el.querySelector("#review-list"+indentifier);
				var template = _.template(this.$("#" + template).html());	
				//初始化垂直列表
				this.listview = new ListView({
					el : listEl,
					itemTemplate : template,
					dataSource : this.datasource,
					autoLoad : autoLoad,
					id : "review-list" + indentifier
				});
			}
		});
});