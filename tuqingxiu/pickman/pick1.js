define([
		'text!pickman/pick1.html',
		'swipe',
		'spin',
		'shared/js/client'
	],
	function(viewTemplate, swipe, Spinner, Client) {
		return Piece.View.extend({
			id: 'pick1',
			events: {
				"touchstart .one_topical": "goToDetail",
			},
			render: function() {
				$(this.el).html(viewTemplate);
				
				Piece.View.prototype.render.call(this);
				return this;
			},
			
			goToDetail: function(el) {
				var me = this;
				Piece.Session.saveObject("isGoBack", true);
				//得到当前对象
				var $target = $(el.currentTarget);
				//得到属性值 有travel  eat   movie   carpool
				var dataValue = $target.attr("data-value");
				this.navigate('/pickman/pick2?dataValue='+dataValue, {
						trigger: true
				});
			}
		}); 

	});