define(['underscore','iscroll', 'zepto'], function(_, iscroll, $){	
	/*var myIscroll = _.extend(iscroll, {
		blurInputMoveScroll:function(element, options){
			var options = options;
			_.extend(options,{
				 probeType: 1
			});
			var iscroll = new IScroll(element,options);
			iscroll.on("scrollStart",function(){		//当滑动iscroll时使inupt、textarea失焦
				console.log("moving")
				$('input').blur();
				$('textarea').blur();
			});
			return iscroll;
		}
	});*/
	var myIscroll = function(element, options) {
		var iscroll = new IScroll(element, options);
		iscroll.on('scrollStart', function() {
			console.log('moving');
			$('input').blur();
			$('textarea').blur();
		});
		return iscroll;
	};
	return myIscroll;
});