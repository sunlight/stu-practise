define(function(require) {
  var v16 = require('components/i18n');
  var v28 = require('components/session');
  var v30 = require('components/store');
  var v32 = require('components/util');
  var v34 = require('components/view');

  return {
    'I18n': v16,
    'Session': v28,
    'Store': v30,
    'Util': v32,
    'View': v34
  };
});