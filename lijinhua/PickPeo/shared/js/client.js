define(['zepto', 'backbone', 'shared/js/notification'], function($, Backbone, Notification) {

	return {
		request: function(paras) {
			var url = location.href;
			var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
			var returnValue;
			for (i = 0; i < paraString.length; i++) {
				var tempParas = paraString[i].split('=')[0];
				var parasValue = paraString[i].split('=')[1];
				if (tempParas === paras)
					returnValue = parasValue;
			}

			if (typeof(returnValue) == "undefined") {
				return "";
			} else {
				return returnValue;
			}
		},

		ajax: function(params) {
			console.log('Call API:' + params.url);

			var me = this;

			var callbacks = _.pick(params, ['success', 'error']);

			var defaults = {
				dataType: "json",
				timeout: 15000,
				error: function() {
					console.log('Call API error');
					callbacks.error(arguments);
				}
			};
			//带有默认值
			params = _.extend(defaults, params);
			//代理回调
			var paramsWithProxy = _.extend(params, {
				success: function(response) {
					callbacks.success(response);
				}
			});

			return $.ajax(paramsWithProxy);

		},
		//判断是否为空
		isNull: function(data) {
			var def = true;
			if (typeof data == "string") {
				data = data.trim();
			}
			if ((typeof data !== "undefined" && data && data.length !== 0) || typeof data === "function") {
				def = false;
			}
			return def;
		}

	} //return
});