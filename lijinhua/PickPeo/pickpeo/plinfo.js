define([
		'text!pickpeo/plinfo.html',
		'pickpeo/reviewList', 'shared/js/client', 'zepto'
	],
	function(viewTemplate, reviewList, Client, $) {
		return Piece.View.extend({
			id: 'review',
			events: {
				"click .icon-left-nav": "goBack"
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			init: function() {
				var me = this;
				//初始化tab内容
				var pldata = Piece.Session.loadObject("eat_dataJson");
				 testJson = eval("(" + pldata + ")"); 
				// dataJson = JSON.parse(pldata);
				var reviews=testJson.reviews;
				var pageViews=testJson.pageViews;
				$(".pltit").append("<a class='pl'>评论"+reviews+"</a><a class='pageViews'>浏览"+pageViews+"</a>")
				var content = $(me.el).find(".review-content");
				me.passengerCars = new reviewList({
					el: content
				});
				me.passengerCars.render();

			}

		}); //view define

	});