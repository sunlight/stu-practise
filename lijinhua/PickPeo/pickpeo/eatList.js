define([
		'text!pickpeo/eatList.html',
		'testDrive/index',
		'shared/js/datasource',
		'listview/ListView',
		'pickpeo/client'
	],
	function(viewTemplate, TestDrive, DataSource, ListView, Client) {
		return Piece.View.extend({
			id: 'eatList',
			events: {
				"click .pl": "pinglunDetail",
				"click #fpic,#spic,#tpic": "big",
				"click .eatDetailsText":"allinfo",
			},
			allinfo:function(el){
				var $target = $(el.currentTarget);
				$target.removeClass('deteatDetailsText');
			},
			render: function() {
				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			big:function(el){
				var $target = $(el.currentTarget);
				var url = $target.attr('src');
				$("body").css({
                 "margin": "0 0",
                 "padding": "0 0",
                 "height":"100%"
                 });
                 $("body").append('<div class="bigimg"><img class="img" src="'+url+'"></div>');
                
                 $(".img").addClass("showBig");

				setTimeout(function(){
					$(".img").removeClass("showBig");
				},1000);

                 $(".bigimg").click(function(event) {
				$(".bigimg").css({
                 "display": "none"
                 });
                 });
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
				if (me.options.from == "eat") {
					url =Client.eat_list;
					template = "eat-template";
				} else if (me.options.from == "movie") {
					url =Client.movie_list;
					template = "movie-template";
				}else if (me.options.from == "travel") {
					url =Client.travel_list;
					template = "travel-template";
				}else if (me.options.from == "carpool") {
					url =Client.carpool_list;
					template = "carpool-template";
				}
				
				// if (me.options.isCarsDetailReturn && me.options.currentIndex == me.options.lastIndex) {//index比较是为了state相互不影响
				// if (me.options.isCarsDetailReturn) {
				// 	me._initListView(me.options.currentIndex, "session", true, url, template);
				// 	Piece.Session.saveObject("isCarsDetailReturn", false);
				// } else {
					me._initListView("session", false, url, template); //打上缓存标志session
					me.listview.reloadData(); //重新load数据，由于标志session所以缓存数据
				//}
				var winHeight=$(window).height();
				winHeight=winHeight-52
				$("#eat-list").css({
					height: winHeight//,
					// bottom:"0px"
				});
			},
			//初始化iscroll列表
			_initListView: function( storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'eat-list',
					storage: storage,
					url: url,
					pageParam: 'pageIndex',

				});


				var mainTemplate = _.template($("#eat-list-template").html());
				var listDiv = mainTemplate({
					id: "eat-list"
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#eat-list");
				var template = _.template(this.$("#" + template).html());
				//初始化垂直列表
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "eat-list" 
				});
			},
			pinglunDetail: function(el) {
				var me = this;
				var $target = $(el.currentTarget);
				var dataJson = $target.attr("data-json");
				me.listview.saveState();
				Piece.Session.saveObject("eat_dataJson", dataJson);
				this.navigate('/pickpeo/plinfo', {
					trigger: true
				});
			},
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});