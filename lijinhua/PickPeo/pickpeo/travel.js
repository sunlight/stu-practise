define([
		'text!pickpeo/travel.html', 'pickpeo/eatList', 'shared/js/client', 'zepto'
	],
	function(viewTemplate, EatList, Client, $) {
		return Piece.View.extend({
			id: 'travel',
			events: {
				"click .icon-left-nav": "goBack"
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				me.init();
			},
			goBack: function() {
				window.history.back();
			},
			init: function() {
				var me = this;
				//初始化tab内容
				// var isCarsDetailReturn = Piece.Session.loadObject("isCarsDetailReturn");
				// Piece.Session.saveObject("isCarsDetailReturn", false);
				var content = $(me.el).find(".travel-content");
				me.passengerCars = new EatList({
					el: content,
					from:"travel"
				});
				me.passengerCars.render();

			}

		}); //view define

	});