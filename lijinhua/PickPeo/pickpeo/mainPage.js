define([
		'text!pickpeo/mainPage.html',
		'swipe',
		'spin',
		'shared/js/client'
	],
	function(viewTemplate, swipe, Spinner, Client) {
		return Piece.View.extend({
			id: 'MainPage',
			events: {
				"click #eat,#movie,#travel,#carpool": "goToDetail",
			},
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},
			goToDetail: function(el) {
				var me = this;
				Piece.Session.saveObject("isGoBack", true);
				var $target = $(el.currentTarget);
				var dataValue = $target.attr("data-value");
				if (dataValue == "eat") {
					this.navigate('/pickpeo/eat', {
						trigger: true
					});
				} else if (dataValue == "movie") {
					this.navigate('/pickpeo/movie', {
						trigger: true
					});
				} else if (dataValue == "travel") {
					this.navigate('/pickpeo/travel', {
						trigger: true
					});
				} else if (dataValue == "carpool") {
					this.navigate('/pickpeo/carpool', {
						trigger: true
					});
				}
			}
		});
	});