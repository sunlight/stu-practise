define([
		'text!pickpeo/reviewList.html',
		'testDrive/index',
		'shared/js/datasource',
		'listview/ListView',
		'pickpeo/client'
	],
	function(viewTemplate, TestDrive, DataSource, ListView, Client) {
		return Piece.View.extend({
			id: 'eatList',
			events: {
				// "click .pl": "pinglunDetail",
			},
			render: function() {
				$(this.el).html(viewTemplate);
				this.onShow();
				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var me = this;
				var url = "";
				var template = "";
					url = Client.review_list;
					template = "review-template";
				
				// if (me.options.isCarsDetailReturn && me.options.currentIndex == me.options.lastIndex) {//index比较是为了state相互不影响
				// if (me.options.isCarsDetailReturn) {
				// 	me._initListView(me.options.currentIndex, "session", true, url, template);
				// 	Piece.Session.saveObject("isCarsDetailReturn", false);
				// } else {
					me._initListView("session", false, url, template); //打上缓存标志session
					me.listview.reloadData(); //重新load数据，由于标志session所以缓存数据
				// }
				var winHeight=$(window).height();
				winHeight=winHeight-68
				$("#eat-list").css({
					height: winHeight
				});
			},
			//初始化iscroll列表
			_initListView: function(storage, autoLoad, url, template) {
				var me = this;
				this.datasource = new DataSource({
					identifier: 'eat-list',
					storage: storage,
					url: url,
					pageParam: 'pageIndex',

				});


				var mainTemplate = _.template($("#review-list-template").html());
				var listDiv = mainTemplate({
					id: "eat-list"
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#eat-list");
				var template = _.template(this.$("#" + template).html());
				//初始化垂直列表
				this.listview = new ListView({
					el: listEl,
					itemTemplate: template,
					dataSource: this.datasource,
					autoLoad: autoLoad,
					id: "eat-list" 
				});
			},
			refresh: function() {
				var me = this;
				setTimeout(function() {
					me.listview.refresh();
				}, 100)
			}

		}); //view define

	});