define([], function() {
	return {
		eat_list: "data/eat.json",
		travel_list:"data/travel.json",
		carpool_list:"data/carpool.json",
		movie_list:"data/movie.json",
		review_list:"data/review.json"
	};
});