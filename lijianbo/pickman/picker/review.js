define([
	'text!picker/review_1.html',
	'shared/js/datasource',
	'listview/ListView',
	'picker/client',
	],
	function(viewTemplate,DataSource,ListView,Client){
		return Piece.View.extend({
			id:'reviewPage',
			events : {
				"click .btn_back" : "goToback",
				"click .review,.review_msg" : "review",
				"touchmove .content" : "disableReviewBox",
			},
			render : function(){
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);		
				return this;
			},
			onShow : function(){
				var me = this;
				var template = "";
				var url = "";

				url = Client.path_review_list;
				template = "reviewList-template";

				me._initListView(1,"session",false,url,template);
				me.listview.reloadData();
				
			},
			_initListView : function(indentifier, storage, autoLoad, url, template){
				var me = this;
				this.datasource = new DataSource({
					indentifier : 'review-list' + indentifier,
					storage : storage,
					url : url,
					pageParam : 'pageIndex'
				});

				var mainTemplate = _.template($("#review-list-template").html());
				var listDiv = mainTemplate({
					id : "review-list" + indentifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#review-list"+indentifier);
				var template = _.template(this.$("#" + template).html());	
				//初始化垂直列表
				this.listview = new ListView({
					el : listEl,
					itemTemplate : template,
					dataSource : this.datasource,
					autoLoad : autoLoad,
					id : "review-list" + indentifier
				});
			},
			goToback :function(){
				window.history.back(-1);
			},
			review : function(){
				var reviewBox = document.getElementById("reviewBox");
				if(!reviewBox){
					var reviewBox = document.createElement("div");
					// reviewBox.css({"position":"absolute","width":"100%","height":"80px"});
					reviewBox.style.position = "absolute";
					reviewBox.style.width = "100%";
					// reviewBox.style.height = "100px";
					reviewBox.style.bottom = "0px";
					// reviewBox.style.background = "";
					reviewBox.className="reviewBox";
					reviewBox.id="reviewBox";
					document.body.appendChild(reviewBox);

					var input = document.createElement("input");
					input.className = "review_input";
					reviewBox.appendChild(input);


					var img = document.createElement("img");
					img.className = "review_click";
					img.src = "../img/chatting_biaoqing_btn_normal.png";
					img.style.width = "30px";
					img.style.height = "30px";
					reviewBox.appendChild(img);

				}
				
			},
			disableReviewBox : function(){
				var reviewBox = document.getElementById("reviewBox");
				if(reviewBox){
					document.body.removeChild(reviewBox);
				}
			}
		});
});