define([
	'text!picker/mainPage.html'
	],function(viewTemplate){
		return Piece.View.extend({
			id:"mainPage",
			events :{
				"touchstart .travel" : "goToTravel",				
			},
			render : function(){
				$(this.el).html(viewTemplate);
				
				Piece.View.prototype.render.call(this);
				return this;
			},
			goToTravel : function(){
				console.info("...");
				this.navigate('/picker/pickman', {
						trigger: true
					});
			}
		});
});