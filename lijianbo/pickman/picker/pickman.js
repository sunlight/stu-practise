define([
	'text!picker/pickman_1.html',
	'shared/js/datasource',
	'listview/ListView',
	'picker/client',
	'shared/js/swiper.min'
	],function(viewTemplate,DataSource,ListView,Client,Swiper){
		return Piece.View.extend({
			id:"travel-list",
			imgArrays : new Array(),
			events : {
				"click .btn_back" : "goToback",
				"click .travel_content" : "showDetial",//显示所有内容
				// "click .swipebox" : "swipeImg"
				"touchstart .reviews" : "goToeviews",//查看评论
				"touchstart #photo_1,#photo_2,#photo_3 ":"viewImgs",//图片放大查看
				"click .swiper-slide" : "disableImg",//隐藏放大的图片
				// "touchmove .viewImg_1" : "changeImg"//切换放大的图片
			},
			render : function(){
				$(this.el).html(viewTemplate);
				// this.onShow();
				Piece.View.prototype.render.call(this);
				
				return this;
			},
			goToback :function(){
				window.history.back(-1);
			},
			onShow : function(){
				
				var me = this;
				var template = "";
				var url = "";
				
				url = Client.path_travel_list;
				template = "travelList-template";

				me._initListView(1,"session",false,url,template);
				me.listview.reloadData();		

			},
			_initListView : function(indentifier, storage, autoLoad, url, template){
				var me = this;
				this.datasource = new DataSource({
					indentifier : 'travel-list' + indentifier,
					storage : storage,
					url : url,
					pageParam : 'pageIndex'
				});

				var mainTemplate = _.template($("#travel-list-template").html());
				var listDiv = mainTemplate({
					id : "travel-list" + indentifier
				});
				$(me.el).append(listDiv);
				var listEl = this.el.querySelector("#travel-list"+indentifier);
				var template = _.template(this.$("#" + template).html());	
				//初始化垂直列表
				this.listview = new ListView({
					el : listEl,
					itemTemplate : template,
					dataSource : this.datasource,
					autoLoad : autoLoad,
					id : "travel-list" + indentifier
				});
			},
			getWidth :function(){//根据屏幕宽度设置每行显示的字数
				var textLeng ;
				var width = window.screen.width;
				console.info(width);
				if(width <= 320){
					textLeng = 38;
				}
				else if(width > 320 && width <= 375){
					textLeng = 45;
				}
				else if(width <=414 && width > 375){
					textLeng = 50;
				}
				return textLeng;
			},
			showDetial : function(event){
				var text = $(event.currentTarget);
				var before_height = text.offset().height;
				if(text.css("-webkit-box-orient") === "vertical"){	

					text.css({"-webkit-box-orient":"inline-axis","display":"inline-block"});
					var after_height = text.offset();
					var before_padding_str = $(".table-list").css("padding-bottom");
					var before_padding  = before_padding_str.substring(0,before_padding_str.length-2);
					// console.info(height);
					$(".table-list").css("padding-bottom",parseInt(before_padding)+(after_height.height-before_height));
					// console.info(parseInt(before_padding)+(after_height.height-before_height));
					// window.scrollTo(100,100);
				}else{
					text.css({"-webkit-box-orient":"vertical","display":"-webkit-box"});
				}
			},
			goToeviews : function(){
				// alert("..");
				console.info("goToDetial");
				this.navigate('/picker/review', {
						trigger: true
					});
			},
			viewImgs : function(el){
				//初始化swipe插件
				


				var clickImg = $(el.currentTarget);

				this.imgArrays = this.getLevelImg(clickImg);	

				$(".content").hide();
				$(".bar").hide(); 
				$(".slider").show();

				var div = document.createElement("div");
				div.className = "swiper-wrapper";
				div.id = "swiper-wrapper";
				document.getElementById("swiper-container").appendChild(div);

				var div1 = document.createElement("div");
				div1.className = "swiper-pagination";
				document.getElementById("swiper-container").appendChild(div1);

				$(".swiper-wrapper").animate({"width":"100%","height":"100%","top":"0px"},"slow");
				this.createImg(div,clickImg.attr("src"));//创建点击的图片
				
				for(var i = 0;i<this.imgArrays.length;i++){

					if(this.imgArrays[i].id !== clickImg.attr("id")){
						this.createImg(div,this.imgArrays[i].src);
					}
				}

				var mySwiper = new Swiper ('.swiper-container', {
				    direction: 'horizontal',
				    loop: true,	    
				    // 如果需要分页器
				    pagination: '.swiper-pagination',
				  });
				
			},
			disableImg : function(){
				var slide = document.getElementById("swiper-container").getElementsByTagName("div");
				// console.info(slide.length);
				if(slide){		
					$(".swiper-wrapper").animate({"width":"20%","height":"0%","top":"20px"},"slow");
					$(".swiper-slide").animate({"width":"0%","height":"0%","top":"0px","left":"0"},"slow");
					document.getElementById("swiper-container").innerHTML = "";
					
					$(".slider").hide();
					$(".content").show();
					$(".bar").show(); 
				}
			},
			getLevelImg : function (nowNode){
				return  nowNode.parent().children("img");
			},
			createImg : function(parent,src){//创建图片

				var divSlide = document.createElement("div");
				divSlide.className = "swiper-slide";
				divSlide.id = "swiper-slide";
				divSlide.style.width = "10px";
				divSlide.style.height = "10px";
				divSlide.style.top = "300px";
				divSlide.style.left = "100px";
				parent.appendChild(divSlide);

				var img = document.createElement("img");
				img.src = src;
				img.style.width = "300px";
				// img.style.marginTop = "-"+img.height/2;
				divSlide.appendChild(img);

				$(".swiper-slide").animate({"width":"100%","height":"100%","top":"0px","left":"0"},"slow");
			}
		});
});