var express = require('express');
var app = express();
var path = require('path');

app.use(express.static(path.resolve('.')));

app.get('/', function(req, res) {
	res.redirect('/picker/index.html');
});


app.listen(process.env.PORT || 3000);
console.log('i am up.');