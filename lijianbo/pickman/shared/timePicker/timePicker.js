define(['zepto', 'underscore', 'backbone', 'text!../timePicker/timePicker.html', 'iscroll', "moment", '../timePicker/timeData', 'css!../timePicker/timePicker'], function($, _, Backbone, viewTemplate, IScroll, Moment, TimeData) {
	var Menu = Backbone.View.extend({
		events: {
			"click #timepicker": "pickcancleClick",

		},
		initialize: function(el) {


			// el 当前页面
			this.render(el);

		},
		render: function(el) {
			var me = this;
			$(this.el).html(viewTemplate);
			$(el).append($(this.el));
		},


		mmyScrolls: null, //分钟iscroll
		dmyScrolls: null, //日scroll
		myScrolls: null, //小时iscroll
		defaultday: null, //初始化日
		defaulthours: null, //初始化小时
		defaultminute: null, //初始化分钟

		initIscroll: function(date, type) {



			var defaultDate = date;
			var today;
			var ho;
			var mi;
			var h = TimeData.hours;
			var m = TimeData.minute;
			if (type === '2') {

				h = TimeData.hours2;


			}



			if (defaultDate === undefined || defaultDate.trim().length < 1) {
				var currentd = this.getDates();
				today = currentd.split(' ')[0];
				ho = currentd.split(' ')[1].split(':')[0];
				mi = currentd.split(' ')[1].split(':')[1];

			} else {
				var sp = defaultDate.split(" ");
				today = sp[0];
				ho = sp[1].split(':')[0];
				mi = sp[1].split(':')[1];

			}


			var mounth = today.split("-")[1];
			$('#pickermonth').html(parseInt(mounth) + '月');



			if (type === '2') {

				if (mi === '00') {
					this.defaultminute = ['00', '30', '00', '30', '00', '30', '00', '30', '00', '30'];
				} else {

					this.defaultminute = ['30', '00', '30', '00', '30', '00', '30', '00', '30', '00'];
				}


			} else {
				this.defaultminute = this.getNewArray(mi, TimeData.minute);

			}
			this.defaulthours = this.getNewArray(ho, h);
			this.defaultday = this.initDay(today);

			this.templateMinute(this.defaultminute); //分钟模版渲染
			this.templateHour(this.defaulthours); // 小时模版渲染
			this.templateDay(this.defaultday); // 日模版渲染
			var me = this;
			$('#ok').bind("click", function() {
				me.okClick();
			});
			$('#cancle').bind("click", function() {
				me.cancleClick();
			});
			// $('#timepicker').bind("click", function() {
			// 	me.pickcancleClick($('#timepicker'));
			// });
		},


		//组合数据数组
		getNewArray: function(hou, array) {
			var newarr = new Array();
			var inde = array.indexOf(hou);



			if (inde > 4) {


				var p = inde - 4;
				var ns = array.slice(0, p);

				var nh = array.slice(p);
				newarr = newarr.concat(nh, ns);


			} else {

				var pr = array.length - (4 - inde);

				var nw = array.slice(pr);
				var nu = array.slice(0, pr);
				newarr = newarr.concat(nw, nu);

			}

			console.log(newarr);
			return newarr;

		},
		//显示时间控件
		showTimePicker: function(date, type) {
			//date 格式 '2014-09-19 19:10'
			this.initIscroll(date, type);
			$("#timepicker").show();

			this.myScrolls.refresh();

			this.mmyScrolls.refresh();



			this.dmyScrolls.refresh();
		},



		templateDay: function(dayData) {



			var dayListTemp = _.template($("#dayTemplate").html(), {
				"rows": dayData
			});


			$("#day").html(" ");
			$("#day").append(dayListTemp);
			var itemc = $(".pickeritem-contents").find('.day');
			var litem = $(itemc[14]).addClass('currents');


			var me = this;

			me.dmyScrolls = new IScroll('#wrappers', {
				momentum: true, //禁用惯性
				bounce: false, //禁用反弹
				probeType: 3,
				mouseWheel: true
			});

			me.dmyScrolls.on('scroll', function() {
				me.dupdatePosition();
			});
			me.dmyScrolls.on('scrollEnd', function() {
				me.dupdatePositionend();
			});
			$(window).resize(function() {
				me.dmyScrolls.refresh();
			});

			me.dmyScrolls.scrollTo(0, -540);
			Piece.Session.saveObject("lastY", -540);
		},
		/*
初始化日期数据
*/
		initDay: function(today) {

			var mou = today.split('-')[0] + '-' + today.split('-')[1];

			$("#time").html(mou);


			var dayArry = new Array();


			for (var j = 14; j >= 1; j--) {
				var ateStr = moment(today).subtract('day', j).format('YYYY-MM-DD');

				dayArry.push(ateStr);
			}
			dayArry.push(today);

			for (var i = 1; i <= 14; i++) {
				var adateStr = moment(today).add('day', i).format('YYYY-MM-DD');


				dayArry.push(adateStr);
			}

			return dayArry;
		},


		//获取当前日期 XXXX-XX-XX；
		getDates: function() {

			var myDate = new Date();
			var currentMonth = myDate.getMonth() + 1;
			var currentDay = myDate.getDate();
			var currentHours = myDate.getHours();
			var currentMinute = myDate.getMinutes();;

			if (currentMonth < 10) {

				currentMonth = "0" + currentMonth;
			}
			if (currentDay < 10) {

				currentDay = "0" + currentDay;
			}
			if (currentHours < 10) {

				currentHours = "0" + currentHours;
			}
			if (currentMinute < 10) {

				currentMinute = "0" + currentMinute;
			}
			var currentDate = myDate.getFullYear() + "-" +
				currentMonth + "-" +
				currentDay + " " + currentHours + ":" + currentMinute;
			console.log(currentDate);

			return currentDate;

		},

		//渲染小时滚轴
		templateHour: function(hoursData) {


			var hoursListTemp = _.template($("#hoursTemplate").html(), {
				"rows": hoursData
			});


			$("#hours").html(" ");
			$("#hours").append(hoursListTemp);
			var itemc = $(".pickeritem-content").find('.hour');
			var litem = $(itemc[4]).addClass('currents');


			var me = this;

			me.myScrolls = new IScroll('#wrappers1', {
				momentum: true, //禁用惯性
				bounce: false, //禁用反弹
				probeType: 3,
				mouseWheel: true
			});

			me.myScrolls.on('scroll', function() {
				me.updatePosition();
			});
			me.myScrolls.on('scrollEnd', function() {
				me.updatePositionend();
			});
			$(window).resize(function() {
				me.myScrolls.refresh();
			});

			me.myScrolls.scrollTo(0, -90);



		},
		//渲染分钟滚轴模版
		templateMinute: function(minuteData) {



			var minuteListTemp = _.template($("#minuteTemplate").html(), {
				"rows": minuteData
			});


			$("#minute").html(" ");
			$("#minute").append(minuteListTemp);
			var itemc = $(".pickeritem-content2").find('.minute');
			var litem = $(itemc[4]).addClass('currents');


			var me = this;

			me.mmyScrolls = new IScroll('#wrappers2', {
				momentum: true, //禁用惯性
				bounce: false, //禁用反弹
				probeType: 3,
				mouseWheel: true
			});

			me.mmyScrolls.on('scroll', function() {
				me.mupdatePosition();
			});
			me.mmyScrolls.on('scrollEnd', function() {
				me.mupdatePositionend();
			});
			$(window).resize(function() {
				me.mmyScrolls.refresh();
			});

			me.mmyScrolls.scrollTo(0, -90);

		},
		//现实日期时间，并传递回调函数里面
		renderTiele: function() {



			var time = this.getCurrentTime();
			var date = $($(".pickeritem-contents").find('.currents')[0]).attr('date');
			var mou = date.split('-')[0] + '-' + date.split('-')[1];
			$("#time").html(mou);
			if (this.callback.iscrollupdate) {


				this.callback.iscrollupdate(this, time);
			}
		},
		//获取当前滚动结束时日期时间
		getCurrentTime: function() {
			var date = $($(".pickeritem-contents").find('.currents')[0]).attr('date');
			var hour = $($(".pickeritem-content").find('.currents')[0]).attr('hour');
			var minute = $($(".pickeritem-content2").find('.currents')[0]).attr('minute');
			var time = date + " " + hour + ":" + minute;

			return time;

		},

		//小时滚轴滚动结束监听
		updatePositionend: function() {

			this.myScrolls.scrollTo(0, -90);
			console.log("h结束滑动");

			this.renderTiele();

		},
		//分钟滚轴滚动结束监听
		mupdatePositionend: function() {


			console.log("m结束滑动");

			this.mmyScrolls.scrollTo(0, -90);
			this.renderTiele();
		},
		//日滚轴滚动结束监听

		dupdatePositionend: function() {


			console.log("结束滑动");
			var currentl = $($(".pickeritem-contents").find('.currents')[0]).index();
			var scrollTo = (currentl - 2) * 45;
			this.dmyScrolls.scrollTo(0, -scrollTo);
			this.dmyScrolls.refresh();

			this.renderTiele();
		},
		//日滚轴滚动监听
		dupdatePosition: function() {

			var lastY = parseInt(Piece.Session.loadObject("lastY"));
			var currentY = this.dmyScrolls.y;



			var cy = lastY - currentY;

			var daylength = $(".pickeritem-contents").find('.day').length;
			if (cy > 44) {


				Piece.Session.saveObject("lastY", currentY);

				var currents = $(".pickeritem-contents").find('.currents');

				$(currents[0]).removeClass('currents');
				$(currents[0]).next().addClass('currents');

				var mounth = $($(currents[0]).next()).attr('mounth');

				$('#pickermonth').html(parseInt(mounth) + '月');

				var currentIndex = $(currents[0]).index();

				var afterel = daylength - currentIndex;
				console.log(daylength + "-----" + afterel);

				if (afterel == 7) {


					var dayArry = new Array();

					var lastday = $($(".pickeritem-contents").find('.day')[daylength - 1]).attr('date');
					console.log("lastday" + lastday);
					for (var i = 1; i <= 7; i++) {


						console.log("新增day");
						var adateStr = moment(lastday).add('day', i).format('YYYY-MM-DD');


						dayArry.push(adateStr);
					}


					var dayListTemp = _.template($("#dayTemplate").html(), {
						"rows": dayArry
					});

					$("#day").append(dayListTemp);
				}
				var currentl = $($(".pickeritem-contents").find('.currents')[0]).index();
				var scrollTo = (currentl - 2) * 45;
				this.dmyScrolls.scrollTo(0, -scrollTo);
			} else if (cy < -44) {
				Piece.Session.saveObject("lastY", currentY);

				var currents = $(".pickeritem-contents").find('.currents');
				$(currents[0]).removeClass('currents');
				$(currents[0]).prev().addClass('currents');


				var mounth = $($(currents[0]).prev()).attr('mounth');

				$('#pickermonth').html(parseInt(mounth) + '月');


				var beforeel = $(currents[0]).index();
				console.log("beforeel" + beforeel);
				if (beforeel == 7) {


					var dayArry = new Array();
					var firstday = $($(".pickeritem-contents").find('.day')[0]).attr('date');
					for (var m = 7; m >= 1; m--) {
						var adateStr = moment(firstday).subtract('day', m).format('YYYY-MM-DD');


						dayArry.push(adateStr);
					}


					var dayListTemp = _.template($("#dayTemplate").html(), {
						"rows": dayArry
					});

					$("#day").prepend(dayListTemp);
				}

				var currentl = $($(".pickeritem-contents").find('.currents')[0]).index();
				var scrollTo = (currentl - 2) * 45;
				this.dmyScrolls.scrollTo(0, -scrollTo);

			}



		},

		//小时滚轴滚动监听
		updatePosition: function() {

			var lastY = -90; // parseInt(Piece.Session.loadObject("lastY"));
			var currentY = this.myScrolls.y;



			var cy = lastY - currentY;


			if (cy > 44) {

				var itemc = $(".pickeritem-content").find('.hour');
				var litem = itemc[0];

				var currents = $(".pickeritem-content").find('.currents');
				$(itemc[4]).removeClass('currents');



				$(itemc[5]).addClass('currents');
				$(litem).remove();
				$(".pickeritem-content").append(litem);


				this.myScrolls.scrollTo(0, -90);
			} else if (cy < -44) {
				console.log("lastY===" + lastY + "   currentY===" + currentY);
				console.log("下拉===" + currentY);


				var itemc = $(".pickeritem-content").find('.hour');
				var litem = itemc[itemc.length - 1];
				var currents = $(".pickeritem-content").find('.currents');

				$(litem).remove();


				$(".pickeritem-content").prepend(litem);
				$(itemc[4]).removeClass('currents');

				$(itemc[3]).addClass('currents');

				this.myScrolls.scrollTo(0, -90);

			}


		},

		//分钟滚轴滚动监听
		mupdatePosition: function() {
			var lastY = -90; // parseInt(Piece.Session.loadObject("lastY"));
			var currentY = this.mmyScrolls.y;



			var cy = lastY - currentY;


			if (cy > 44) {



				var itemc = $(".pickeritem-content2").find('.minute');
				var litem = itemc[0];

				var currents = $(".pickeritem-content2").find('.currents');
				$(itemc[4]).removeClass('currents');



				$(itemc[5]).addClass('currents');
				$(litem).remove();
				$(".pickeritem-content2").append(litem);


				this.mmyScrolls.scrollTo(0, -90);

			} else if (cy < -44) {



				var itemc = $(".pickeritem-content2").find('.minute');
				var litem = itemc[itemc.length - 1];
				var currents = $(".pickeritem-content2").find('.currents');

				$(litem).remove();


				$(".pickeritem-content2").prepend(litem);
				$(itemc[4]).removeClass('currents');

				$(itemc[3]).addClass('currents');
				this.mmyScrolls.scrollTo(0, -90);
			}

		},
		//取消按钮
		cancleClick: function() {

			$("#timepicker").hide();



		},

		displayPicker: function(e) {
			console.log("displayPicker");

		},
		pickcancleClick: function(e) {



			var el = $(e.currentTarget);
			var me = this;


			if ($(e.toElement).hasClass('displayPicker')) {

				console.info("you");

				var time = this.getCurrentTime();
				if (this.callback.ok) {
					this.callback.ok(this, time);
				}
				$("#timepicker").hide();

			} else {

				console.info("wu");
			}



		},

		hideTimePicker: function() {

			$("#timepicker").hide();

		},
		//确定按钮监听，将当前显示时间传递回调函数里面
		okClick: function() {

			var time = this.getCurrentTime();
			if (this.callback.ok) {


				this.callback.ok(this, time);
			}

		},
		//回调函数 ，ok按钮点击，iscrollupdate滚轴滚动

		callback: {
			ok: function(view, result) {},
			iscrollupdate: function(view, result) {},

		},
	});

	return Menu;
});